/** @type {import('next').NextConfig} */

const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    images: {
        remotePatterns: [
            {
                protocol: "https",
                hostname: "rabingolemio.blob.core.windows.net",
            },
            {
                protocol: "https",
                hostname: "golemgolemio.blob.core.windows.net",
            },
            {
                protocol: "https",
                hostname: "img.icons8.com",
            },
            {
                protocol: "https",
                hostname: "publications.europa.eu",
            },
            {
                protocol: "https",
                hostname: "www.mmdecin.cz",
            },
            {
                protocol: "https",
                hostname: "lkod-ftp.portabo.cz",
            },
            {
                protocol: "https",
                hostname: "kr-ustecky.brandcloud.pro",
            },

        ],
        minimumCacheTTL: 120,
        deviceSizes: [828, 1200],
        formats: ["image/webp"],
    },
    i18n: {
        locales: ["cs-CZ"],
        defaultLocale: "cs-CZ",
    },
    logging: {
        fetches: {
            fullUrl: true,
        },
    },
    async redirects() {
        return [
            {
                source: "/dataset/:path*",
                destination: "/datasets",
                permanent: true,
            },
            {
                source: "/organization/:path*",
                destination: "/organizations",
                permanent: true,
            },
            {
                source: "/group/:path*",
                destination: "/",
                permanent: true,
            },
            {
                source: "/about",
                destination: "/project",
                permanent: true,
            },
        ];
    },
};

// for usual case
module.exports = nextConfig;
