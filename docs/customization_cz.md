# Uživatelská konfigurace Lokálního katalogu otevřených dat (Katalog)

## Zdroj dat

- pro ziskání dat je nově možné použít API endpointy s JSON databází místo SPARQL databáze
- nastavení se provádí pře ENV proměnnou `DATA_SOURCE`, kde je možné nastavit `sparql` nebo `json`, defaultní hodnota je nastavena na `json` v souboru `src/config.ts`
- adresu k API endpointu nastavíte stejně jako pro SPARQL endpoint v env proměnné `ENDPOINT`
- pokud chcete použít JSON databázi, je nutné nastavit adresu k úložišti ikon pro rozlišení datasetů, které databáze neobsahuje, přes ENV proměnnou `ICONS_URL`, ikony musí být ve formátu svg s názvem odpovídajícím tématu datasetu, např.: `ENVI.svg, REGI.svg`

## Logo a přilehlý text

### Hlavní titulek

- text změníte v souboru `src/app/(textContent)/cs.json` sekce `configurable/siteTitle`

### Kde měnit obrázky a loga?

#### Favicon

- favicon: složka `public/`
  - nový favicon si můžete vygenerovat zdarma například na `https://favicon.io/` a poté vygenerované soubory nahrát do výše uvedené složky
  - pokud ikonu nezměníte, stránka bude mít v tabu prohlížeče ikonu Hl. m. P.

#### Logo

- logo: soubor `public/assets/images/logos/logoOwner.svg`
- text vedle loga změníte v souboru `src/app/(textContent)/cs.json` sekce `configurable/logoText`
- komponenta `src/app/(components)/configurable/Logo/index.tsx`
  - pokud pojmenujete obrázek stejně, zde nic neměníte

#### Backgound image

- pozadí na homepage: soubor `public/assets/images/mainBackgroundImage.png`
- pozadí na poject page: soubor `public/assets/images/projectPageBackgroudImage.png`
- komponenta `src/app/(components)/configurable/BackgroundPicture/index.tsx`
  - pokud pojmenujete obrázky stejně, zde nic neměníte

## O katalogu na homepage (podkapitola)

- text změníte v souboru `src/app/(textContent)/cs.json` sekce `aboutCatalogText`
- obrázek je nalinkován stejný jako na pozadí stránky, pokud chete použít jiný, změny proveďte v souboru `src/app/(components)/ProjectInfo/index.tsx`
- pokud nechcete používat stránku _O katalogu_, kde můžete přidat informace o vás, deaktivujte odkaz pomocí ENV proměnné `SHOW_PROJECT_PAGE=false`

## O katalogu page

- text změníte v souboru `src/app/(textContent)/cs.json` sekce `projectpage`
- pokud potřebujete změnit strukturu stránky, úpravy proveďte v souboru `src/app/about-open-data/page.tsx`
- pokud nechcete používat stránku _O katalogu_, kde můžete přidat informace o vás, deaktivujte odkaz v horní liště navigace pomocí ENV proměnné `SHOW_PROJECT_PAGE=false`

## O lkodu page

- text změníte v souboru `src/app/(textContent)/cs.json` sekce `aboutLkodPage`, text pro anglickou verzi změníte v souboru `src/app/(textContent)/en.json` sekce `aboutLkodPage`
- pokud potřebujete změnit strukturu stránky, úpravy proveďte v souboru `src/app/about-lkod/page.tsx`
- pokud nechcete používat stránku _O lkodu_, kde můžete přidat informace o vás, deaktivujte odkaz v horní liště navigace pomocí ENV proměnné `SHOW_ABOUTLKOD_PAGE=false`

## Data Praha page v navigaci

- odkaz v horní liště navigace na stránku můžete deaktivovat pomocí ENV proměnné `SHOW_DATAPRAHA_PAGE=false`

## Footer

- odkazy na sociální sítě změníte v souboru `src/app/(components)/Footer/Socials/index.tsx`
- v případě, že používáte pouze modul katalog, deaktivujte prosím odkaz na přihlášení do administrace _Přihlásit se_ pomocí ENV proměnné `SHOW_ADMINLOGIN_LINK=false`

## Meta a open graph tagy

- meta tagy a open graph tagy k projektu i jednotlyvým stránkám můžete přizpůsobit v souboru `src/metatags.json`
  - POZOR: mimo homepage a cookies, hodnota `title` nastavuje u jednotlivých stránek název stránky, meta tag title i titulek v hlavičce stránky a hodnotu v breadcrumbs

## Kde měnit barvy?

- použité barvy lze měnit v souboru `src/styles/_variables.scss`
