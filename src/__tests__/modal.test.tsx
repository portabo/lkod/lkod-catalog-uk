import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { Modal } from "@/components/Modal";

import MockLayout from "./MockLayout";

describe("Modal component", () => {
    it("renders modal", async () => {
        const clicked = jest.fn();
        render(
            <MockLayout>
                <Modal show onClose={clicked} label="label" titleData="title">
                    <div>children</div>
                </Modal>
            </MockLayout>
        );

        const modal = screen.getByTestId("dialog");
        expect(modal).toBeInTheDocument();
        expect(modal).toHaveTextContent("children");

        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        expect(button).toHaveAttribute("aria-label", "Zavřít");

        const heading = screen.getByRole("heading", { level: 4, name: "label" });
        expect(heading).toBeInTheDocument();

        fireEvent.click(button);
        expect(clicked).toHaveBeenCalledTimes(1);
        fireEvent.click(button);
        expect(clicked).toHaveBeenCalledTimes(2);

        const titleSpan = heading.nextElementSibling;

        expect(titleSpan).toHaveClass("modal__title-data");
        expect(titleSpan).toHaveTextContent("title");
    });
});
