import "@testing-library/jest-dom";

import { render, screen } from "@testing-library/react";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";

describe("Breadcrumb component", () => {
    it("renders breadcrumb with home link and svg icon", () => {
        render(<Breadcrumb labels={[{ label: "Home", link: "/" }]} />);

        const breadcrumbs = screen.getByRole("list");
        expect(breadcrumbs).toBeInTheDocument();
        const breadcrumbsItems = screen.getAllByRole("listitem");
        const homeLi = breadcrumbsItems[0];
        expect(homeLi).toBeInTheDocument();
        const homeLink = screen.getByRole("link", { name: "Domů" });
        expect(homeLink).toBeInTheDocument();
        expect(homeLink).toHaveAttribute("href", "/");
        expect(homeLink).toHaveAttribute("aria-label", "Domů");
        const homeIcon = homeLink.querySelector("svg");
        expect(homeIcon).toBeInTheDocument();
        expect(homeIcon).toHaveAttribute("style", "width: 18px; height: 20px;");
    });

    it("renders list items with correct links and labels", () => {
        const labels = [
            { link: "/link1", label: "Label 1" },
            { link: "/link2", label: "Label 2" },
            { link: "/link3", label: "Label 3" },
        ];

        render(<Breadcrumb labels={labels} />);

        const listItems = screen.getAllByRole("listitem");
        expect(listItems).toHaveLength(labels.length + 1);

        labels.forEach((label, i) => {
            const element = screen.getByText(label.label);
            expect(element).toBeInTheDocument();
            if (i === labels.length - 1) {
                expect(element).not.toHaveAttribute("href");
                expect(element.querySelector("a")).toBeNull();
            } else {
                expect(element).toHaveAttribute("href", label.link);
                expect(element).toHaveAttribute("aria-label", label.label);
            }
        });
    });
});
