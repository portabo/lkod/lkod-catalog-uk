import { render, screen } from "@testing-library/react";
import React from "react";

import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";

describe("NotificationBanner", () => {
    it("renders info notification", () => {
        render(<NotificationBanner type={NotificationType.Info} text="Info message" />);

        const banner = screen.getByText("Info message");
        expect(banner).toBeInTheDocument();
        expect(banner.closest("div")).toHaveClass("color-info");
    });

    it("renders warning notification", () => {
        render(<NotificationBanner type={NotificationType.Warning} text="Warning message" />);

        const banner = screen.getByText("Warning message");
        expect(banner).toBeInTheDocument();
        expect(banner.closest("div")).toHaveClass("color-warning");
    });

    it("renders error notification", () => {
        render(<NotificationBanner type={NotificationType.Error} text="Error message" />);

        const banner = screen.getByText("Error message");
        expect(banner).toBeInTheDocument();
        expect(banner.closest("div")).toHaveClass("color-error");
    });
});
