import "@testing-library/jest-dom";

import { render, screen } from "@testing-library/react";
import React from "react";

import Page from "@/accessibility/page";
import Meta from "@/metatags.json";
import text from "@/textContent/cs.json";

describe("Accessibility Page", () => {
    it("renders a h1 heading", () => {
        render(<Page />);

        const heading = screen.getByRole("heading", { level: 1, name: `${Meta.accessibilityPage.title}` });

        expect(heading).toBeInTheDocument();
    });

    it("renders a link to the Golemio email", () => {
        render(<Page />);

        const link = screen.getByRole("link", { name: `${text.accessibility.golemioEmail}` });

        expect(link).toBeInTheDocument();
    });

    it("renders a list of links", () => {
        render(<Page />);

        const links = screen.getAllByRole("link");

        expect(links).toHaveLength(4);
    });

    it("renders a list of headings", () => {
        render(<Page />);

        const headings = screen.getAllByRole("heading");

        expect(headings).toHaveLength(5);
    });
});
