import "@testing-library/jest-dom";

import { render, screen } from "@testing-library/react";
import React from "react";

import Button from "@/components/Button";

describe("Button component", () => {
    it("renders button with label", () => {
        render(<Button color="primary" label="Label" />);

        const button = screen.getByRole("button");
        expect(button).toHaveAttribute("type", "button");
        expect(button).toBeInTheDocument();
        expect(button).toHaveTextContent("Label");
    });

    it("renders button with hidden label on mobile", () => {
        render(<Button color="primary" label="Label" hideLabelMobile />);

        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        expect(button).toHaveAttribute("aria-label", "Label");
        const label = button.querySelector("span");
        expect(label).toHaveClass("label");
    });

    it("renders button with start icon", () => {
        render(<Button color="primary" label="Label" iconStart={<svg />} />);

        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        const icon = button.querySelector("svg");
        expect(icon).toBeInTheDocument();
    });

    it("renders button with end icon", () => {
        render(<Button color="primary" label="Label" iconEnd={<svg />} />);

        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        const icon = button.querySelector("svg");
        expect(icon).toBeInTheDocument();
    });

    it("renders disabled button", () => {
        render(<Button color="primary" label="Label" disabled />);

        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        expect(button).toBeDisabled();
    });

    it("renders button with hidden label", () => {
        render(<Button color="primary" label="Label" hideLabel />);

        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        const label = button.querySelector("span");
        expect(label).toBeNull();
    });
});
