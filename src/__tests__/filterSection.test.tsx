import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { FilterFormSection } from "@/modules/(filters)/FilterFormSection";

describe("FilterFormSection component", () => {
    beforeEach(() => {
        const limit = 3;
        render(
            <FilterFormSection
                className="className"
                checkerCondition="iri2"
                data={[
                    { iri: "iri1", label: "label", count: "count" },
                    { iri: "iri2", label: "label", count: "count" },
                    { iri: "iri3", label: "label", count: "count" },
                    { iri: "iri4", label: "label", count: "count" },
                    { iri: "iri5", label: "label", count: "count" },
                    { iri: "iri6", label: "label", count: "count" },
                ]}
                fnc={() => {}}
                label="label"
                limit={limit}
                section="section"
            />
        );
    });

    it("checks if the filter form section is rendered correctly", () => {
        const limit = 3;
        const filterFormSection = screen.getByRole("group");
        expect(filterFormSection).toBeInTheDocument();
        const checkboxes = screen.getAllByRole("checkbox");
        expect(checkboxes).toHaveLength(limit);
    });

    it("checks clicking button 'Zobrazit další' shows more filters", () => {
        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        expect(button).toHaveTextContent("Zobrazit další");

        fireEvent.click(button);
        expect(screen.getAllByRole("checkbox")).toHaveLength(6);
        expect(button).toHaveTextContent("Zobrazit první");
    });

    it("checks clicking button 'Zobrazit první' shows less filters", () => {
        const button = screen.getByRole("button");
        expect(button).toBeInTheDocument();
        fireEvent.click(button);
        expect(button).toHaveTextContent("Zobrazit první");

        fireEvent.click(button);
        expect(screen.getAllByRole("checkbox")).toHaveLength(3);
    });
});

describe("FilterFormSection checkbox test", () => {
    it("checks clicking checkbox calls the function", () => {
        const limit = 3;
        let value = "";
        render(
            <FilterFormSection
                className="className"
                checkerCondition="iri3"
                data={[
                    { iri: "iri1", label: "label", count: "count" },
                    { iri: "iri2", label: "label", count: "count" },
                    { iri: "iri3", label: "label", count: "count" },
                    { iri: "iri4", label: "label", count: "count" },
                    { iri: "iri5", label: "label", count: "count" },
                    { iri: "iri6", label: "label", count: "count" },
                ]}
                fnc={(iri) => {
                    value = iri;
                }}
                label="label"
                limit={limit}
                section="section"
            />
        );
        const checkboxes = screen.getAllByRole("checkbox");
        fireEvent.click(checkboxes[1]);
        expect(value).toBe("iri2");

        fireEvent.click(checkboxes[0]);
        expect(value).toBe("iri1");
    });
});
