import "@testing-library/jest-dom";

import { render, screen } from "@testing-library/react";
import React from "react";

import { Counter } from "@/(home)/(counter)/Counter";

describe("Counter component", () => {
    it("renders counter", async () => {
        // Define mock implementations for the methods
        const mockOrganizations = jest.fn().mockResolvedValue([{ count: 5 }, { count: 0 }, { count: 3 }]);
        const mockThemes = jest.fn().mockResolvedValue([{ id: 1 }, { id: 2 }, { id: 3 }]);
        const mockDatasetsCount = jest.fn().mockResolvedValue({ count: 10 });

        // Fetch the mock data
        const orgs = await mockOrganizations();
        const themes = await mockThemes();
        const datasets = await mockDatasetsCount();

        const pregnantOrgs = orgs.filter((org: { count: number }) => org.count > 0);

        render(<Counter datasetsCount={datasets.count} themesCount={themes.length} orgsCount={pregnantOrgs.length} />);

        const counter = screen.getByTestId("counter");
        expect(counter).toBeInTheDocument();

        const links = counter.querySelectorAll("a");
        expect(links).toHaveLength(3);

        const orgLink = links[0];
        expect(orgLink).toHaveTextContent("2");
        expect(orgLink).toHaveTextContent("organizací");
        expect(orgLink).toHaveAttribute("href", "/organizations");

        const datasetLink = links[1];
        expect(datasetLink).toHaveTextContent("10");
        expect(datasetLink).toHaveTextContent("datových sad");
        expect(datasetLink).toHaveAttribute("href", "/datasets");

        const themeLink = links[2];
        expect(themeLink).toHaveTextContent("3");
        expect(themeLink).toHaveTextContent("témat");
        expect(themeLink).toHaveAttribute("href", "#topicList");
    });
});
