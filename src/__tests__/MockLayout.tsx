import "@testing-library/jest-dom";

import { render, screen } from "@testing-library/react";
import React, { ReactNode } from "react";

const MockLayout = ({ children }: { children: ReactNode }) => {
    return (
        <div>
            <div id="modal-root"></div>
            {children}
        </div>
    );
};

describe("Mock Layout", () => {
    it("renders children", () => {
        const children = <div>children</div>;
        render(<MockLayout>{children}</MockLayout>);

        const div = screen.getByText("children");
        expect(div).toBeInTheDocument();
    });
});

export default MockLayout;
