export interface Config {
    DATA_SOURCE: "json" | "sparql";
    ENDPOINT: string;
    ICONS_URL: string;
    environment: "production" | "development" | "test";
    PUBLIC_URL: string;
    ADMIN_URL: string;
    GOOGLE_SITE_VERIFICATION: string;
    SHOW_PROJECT_PAGE: string;
    SHOW_ABOUTLKOD_PAGE: string;
    SHOW_ADMINLOGIN_LINK: string;
    SHOW_DATAPRAHA_PAGE: string;
    LOG_LEVEL: string;
    NOTIFICATION_BANNER_TEXT: string;
    NOTIFICATION_BANNER_SEVERITY: "info" | "warning" | "error";
}

export const config: Config = {
    DATA_SOURCE: (process.env.DATA_SOURCE as "json" | "sparql") ?? ("json" as "json" | "sparql"),
    ENDPOINT: process.env.ENDPOINT ?? "",
    ICONS_URL: process.env.ICONS_URL ?? "",
    environment: process.env.NODE_ENV,
    PUBLIC_URL: process.env.PUBLIC_URL ?? "",
    ADMIN_URL: process.env.ADMIN_URL ?? "",
    GOOGLE_SITE_VERIFICATION: process.env.GOOGLE_SITE_VERIFICATION ?? "",
    SHOW_PROJECT_PAGE: process.env.SHOW_PROJECT_PAGE ?? "true",
    SHOW_ABOUTLKOD_PAGE: process.env.SHOW_ABOUTLKOD_PAGE ?? "true",
    SHOW_ADMINLOGIN_LINK: process.env.SHOW_ADMINLOGIN_LINK ?? "true",
    SHOW_DATAPRAHA_PAGE: process.env.SHOW_DATAPRAHA_PAGE ?? "true",
    LOG_LEVEL: process.env.LOG_LEVEL ?? "info",
    NOTIFICATION_BANNER_TEXT: process.env.NOTIFICATION_BANNER_TEXT ?? "",
    NOTIFICATION_BANNER_SEVERITY: (process.env.NOTIFICATION_BANNER_SEVERITY as "info" | "warning" | "error") ?? "",
};
