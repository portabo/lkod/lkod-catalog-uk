import "@/styles/reset.css";
import "@/styles/globals.scss";

import { Metadata } from "next";
import PlausibleProvider from "next-plausible";
import { ReactNode } from "react";
import { config } from "src/config";

import Footer from "@/components/Footer";
import Header from "@/components/Header";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import OrganizationsProvider from "@/context/OrganizationsProvider";
import { roboto } from "@/fonts/fonts";
import { getLogger } from "@/logging/logger";
import Meta from "@/metatags.json";
import { CatalogService } from "@/services/catalog-service";
import text from "@/textContent/cs.json";
import { getErrorMessage } from "@/utils/getErrorMessage";

import styles from "./layout.module.scss";

const logger = getLogger("RootLayout");

const domain = config.PUBLIC_URL;
let plausibleDomain: string;

if (domain) {
    try {
        const url = new URL(domain);
        plausibleDomain = url.host;
    } catch (error) {
        logger.error({
            provider: "server",
            function: "plusibleDomain setting",
            component: "app-layout",
            message: `Invalid URL: ${domain}`,
            error: getErrorMessage(error),
        });
    }
}

const metadataBase = new URL(domain);

export const metadata: Metadata = {
    metadataBase,
    title: text.configurable.siteTitle,
    description: Meta.appDescription,
    verification: {
        google: config.GOOGLE_SITE_VERIFICATION,
    },
    openGraph: {
        title: text.configurable.siteTitle,
        description: Meta.appDescription,
        images: ["/assets/images/ogImages/lkod_homepage.png"],
    },
    twitter: {
        card: "summary_large_image",
        title: text.configurable.siteTitle,
        description: Meta.appDescription,
        images: ["/assets/images/ogImages/lkod_homepage_tw.png"],
    },
};

logger.info({ provider: "server", component: "app-layout", message: "App started" });

export type NavBarConfig = {
    showProjectPage: string;
    showAboutLkodPage: string;
    showDataPrahaPage: string;
};

const navBarConfig: NavBarConfig = {
    showProjectPage: config.SHOW_PROJECT_PAGE,
    showAboutLkodPage: config.SHOW_ABOUTLKOD_PAGE,
    showDataPrahaPage: config.SHOW_DATAPRAHA_PAGE,
};

export const dynamic = "force-dynamic";

const RootLayout = ({ children }: { children: ReactNode }) => {
    const organizationsPromise = CatalogService.getAllPublishersData();
    return (
        <html lang="cs" className={`${roboto.variable}`}>
            <head>
                <PlausibleProvider domain={plausibleDomain} trackOutboundLinks trackFileDownloads />
                <link rel="canonical" href={`${config.PUBLIC_URL}`} />
            </head>
            <body className={`${styles["main-layout"]}`}>
                {config.NOTIFICATION_BANNER_SEVERITY ? (
                    <NotificationBanner
                        type={config.NOTIFICATION_BANNER_SEVERITY as NotificationType}
                        text={config.NOTIFICATION_BANNER_TEXT}
                    />
                ) : null}
                <OrganizationsProvider organizationsPromise={organizationsPromise}>
                    <Header navBarConfig={navBarConfig} />
                    <div id="modal-root" />
                    {children}
                    <Footer adminDomain={config.ADMIN_URL} showAdminLink={config.SHOW_ADMINLOGIN_LINK} />
                </OrganizationsProvider>
            </body>
        </html>
    );
};

export default RootLayout;
