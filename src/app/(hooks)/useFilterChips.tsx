"use client";
/* eslint-disable react-hooks/exhaustive-deps */
import isDeepEqual from "fast-deep-equal/es6/react";
import { use, useEffect, useRef, useState } from "react";

import { IListFormat, IListKeyword, IListPublisher, IListTheme } from "@/(schema)";
import { useFormatsPromise, useKeywordsPromise, usePublishersPromise, useThemesPromise } from "@/context/DataProvider";
import { getLogger } from "@/logging/logger";

import { useUrlFilter } from "./useUrlFilter";

const logger = getLogger("useFilterChips");

export const useFilterChips = () => {
    const publishersData = use(usePublishersPromise());
    const topicsData = use(useThemesPromise());
    const keywordsData = use(useKeywordsPromise());
    const formatsData = use(useFormatsPromise());
    const { urlFilter } = useUrlFilter();

    const [publishers, setPublishers] = useState<IListPublisher[]>();
    const [topics, setTopics] = useState<IListTheme[]>();
    const [keys, setKeys] = useState<IListKeyword[]>();
    const [formats, setFormats] = useState<IListFormat[]>();

    const { publisherIri, themeIris, keywords, formatIris } = urlFilter;

    const keywordsRef = useRef(keywords);
    const themeIrisRef = useRef(themeIris);
    const formatIrisRef = useRef(formatIris);

    if (!isDeepEqual(keywordsRef.current, keywords)) {
        keywordsRef.current = keywords;
    }
    if (!isDeepEqual(themeIrisRef.current, themeIris)) {
        themeIrisRef.current = themeIris;
    }

    if (!isDeepEqual(formatIrisRef.current, formatIris)) {
        formatIrisRef.current = formatIris;
    }

    const getPublisherChip = (queryPublisher?: string) => {
        let publishers: IListPublisher[];
        if (!queryPublisher || publishersData.length === 0) {
            publishers = [];
        } else {
            publishers = [publishersData[publishersData.findIndex((element: IListPublisher) => element.iri === queryPublisher)]];
        }
        logger.debug({ provider: "client", function: "getPublisherChip", message: `publishers count: ${publishers.length}` });
        setPublishers(publishers);
    };

    const getTopicChips = (queryTopic?: string | string[]) => {
        let topics: IListTheme[];
        if (!queryTopic) {
            topics = [];
        } else if (typeof queryTopic === "string") {
            topics = [topicsData[topicsData.findIndex((element) => element.iri === queryTopic)]];
        } else {
            topics = queryTopic.map((el) => topicsData.filter((e) => e.iri === el)).flat(1);
        }
        logger.debug({ provider: "client", function: "getTopicChips", message: `topics count: ${topics.length}` });
        setTopics(topics);
    };

    const getKeys = (queryKey?: string | string[]) => {
        let keywords: IListKeyword[];
        if (!queryKey) {
            keywords = [];
        } else if (typeof queryKey === "string") {
            keywords = [keywordsData[keywordsData.findIndex((element) => element.label === queryKey)]];
        } else {
            keywords = queryKey.map((el) => keywordsData.filter((e) => e.label === el)).flat(1);
        }
        logger.debug({ provider: "client", function: "getKeys", message: `keywords count: ${keywords.length}` });
        setKeys(keywords);
    };

    const getFormatChips = (queryFormat?: string | string[]) => {
        let formats: IListFormat[];
        if (!queryFormat) {
            formats = [];
        } else if (typeof queryFormat === "string") {
            formats = [formatsData[formatsData.findIndex((element) => element.iri === queryFormat)]];
        } else {
            formats = queryFormat.map((el) => formatsData.filter((e) => e.iri === el)).flat(1);
        }
        logger.debug({ provider: "client", function: "getFormatChips", message: `formats count: ${formats.length}` });
        setFormats(formats);
    };

    useEffect(() => {
        getPublisherChip(publisherIri);
        getTopicChips(urlFilter.themeIris);
        getKeys(urlFilter.keywords);
        getFormatChips(urlFilter.formatIris);
    }, [publisherIri, themeIrisRef.current, keywordsRef.current, formatIrisRef.current]);

    return { topics, publishers, keys, formats };
};
