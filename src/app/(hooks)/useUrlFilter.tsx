"use client";
import { useSearchParams } from "next/navigation";

import { genericFilter, IFilter } from "@/types/FilterInterface";
import { stringOrArr } from "@/utils/helpers";

export const useUrlFilter = () => {
    const publisherIri = useSearchParams()?.get("publisherIri");
    const themeIris = useSearchParams()?.getAll("themeIris");
    const keywords = useSearchParams()?.getAll("keywords");
    const formatIris = useSearchParams()?.getAll("formatIris");
    const search = useSearchParams()?.get("search");

    const urlFilter: genericFilter & IFilter = {
        publisherIri: publisherIri ? publisherIri : "",
        themeIris: themeIris ? stringOrArr(themeIris) : [],
        keywords: keywords ? stringOrArr(keywords) : [],
        formatIris: formatIris ? stringOrArr(formatIris) : [],
    };

    const searchString = search ? search : "";

    return { urlFilter, searchString };
};
