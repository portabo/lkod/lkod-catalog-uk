import parse from "html-react-parser";
import type { NextPage } from "next";
import { Metadata } from "next";
import Link from "next/link";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Meta from "@/metatags.json";
import text from "@/textContent/cs.json";

import styles from "./Accessibility.module.scss";

const PAGE_LABEL = Meta.accessibilityPage.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.accessibilityPage.title}`,
};

const Project: NextPage = () => {
    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <section className={styles["text"]}>
                <p>{text.accessibility.accessibilityDescription}</p>
                <p>{parse(text.accessibility.accessibilityDescriptionP2)}</p>
                <Heading tag={`h2`}>{text.accessibility.complianceStatus}</Heading>
                <p>{text.accessibility.complianceStatusDescription}</p>
                <Heading tag={`h2`}>{text.accessibility.accessibilityStatementTitle}</Heading>
                <p>{text.accessibility.accessibilityStatementDescription}</p>
                <p>{text.accessibility.accessibilityStatementDescriptionP2}</p>
                <p>{text.accessibility.accessibilityStatementDescriptionP3}</p>
                <Heading tag={`h2`}>{text.accessibility.feedbackAndContactTitle}</Heading>
                <p>
                    {text.accessibility.feedbackAndContactDescription}
                    <Link href={`mailto:${text.accessibility.golemioEmail}`}> {text.accessibility.golemioEmail}</Link>
                </p>
                <Heading tag={`h2`}>{text.accessibility.lawEnforcementTitle}</Heading>
                <p>{text.accessibility.lawEnforcementDescription}</p>
                <p>{text.accessibility.lawEnforcementDescriptionP2} </p>
                <p>{text.accessibility.lawEnforcementDescriptionP3} </p>
                <p>
                    email: <Link href={`mailto:${text.accessibility.email}`}>{text.accessibility.email}</Link>
                </p>
            </section>
        </Container>
    );
};

export default Project;
