/* eslint-disable max-len */
export const examples = [
    {
        title: "PID polohy",
        description:
            "Projekt Sledování poloh PID využívá nástroje Datové platformy Golemio, zejména formou zajištění zpracování a poskytování polohových dat v pražské integrované dopravě a vytvoření rozhraní pro odjezdové tabule a mobilní aplikace. Cestující se tak ihned dozví, jestli vybraný spoj jede načas nebo má prodlení. Díky open API jsou data využívají i aplikace třetích stran, například Google Maps.",
        link: "https://mapa.pid.cz",
        img: "/assets/images/examples/pid.webp",
    },
    {
        title: "Atlas životního prostředí",
        description:
            "Atlas životního prostředí v Praze je soubor webových mapových aplikací, které prezentují dostupné informace o stavu a ochraně životního prostředí v hl. m. Praze. Tematicky je rozčleněn do oblastí ovzduší, krajina, voda, hluk a odpady. Mapy jsou pravidelně aktualizovány na základě údajů od Magistrátu hl. m. Prahy, Institutu plánování a rozvoje hl. m. Prahy a dalších městských a státních organizací.",
        link: "https://www.geoportalpraha.cz/cs/atlas-zivotniho-prostredi",
        img: "/assets/images/examples/atlas_zivotniho_prostredi.webp",
    },
    {
        title: "Praha dopravní",
        description:
            "Jedná se o prototyp webové aplikace, která zobrazuje různá časoprostorová data o dopravě na důležitých silnicích v Praze a Středočeském kraji. Data pocházejí z datové platformy Golemio, Ředitelství silnic a dálnic, Technické správy komunikací a Waze for Cities. Aplikace umožňuje sledovat aktuální i minulý stav dopravy, prezentovat a analyzovat dopravní data pro různé cílové skupiny a podporovat spolupráci mezi městskými subjekty, univerzitami a dalšími partnery.",
        link: "https://golemio.cz/projects",
        img: "/assets/images/examples/praha_dopravni.webp",
    },
    {
        title: "Hlídač státu",
        description:
            "Hlídač státu je webová platforma, kde na jednom místě probíhá kontrola, analýza a propojování dat z registru smluv, veřejných zakázek, dotací, sponzorů politických stran i jednání politiků samotných.",
        link: "https://www.hlidacstatu.cz",
        img: "/assets/images/examples/hlidac_statu.webp",
    },
    {
        title: "Brněnské brownfieldy",
        description: "Mapová aplikace vytvořená z otevřené datové sady Brownfields.",
        link: "https://data.brno.cz/pages/brownfield",
        img: "/assets/images/examples/brnenske_brownfieldy.webp",
    },
    {
        title: "Jak se změnil počet obyvatel vaší obce?",
        description:
            "Otevřená data využívá také datová žurnalistika.  Seznam Zprávy na základě dat zmapovaly, jak se v posledních 150 letech vyvíjely počty obyvatel v jednotlivých obcích",
        link: "https://www.seznamzpravy.cz/clanek/fakta-od-cisare-pana-po-soucasnost-podivejte-se-jak-se-zmenil-pocet-obyvatel-vasi-obce-204823",
        img: "/assets/images/examples/pocet_obyvatel.webp",
    },
];
