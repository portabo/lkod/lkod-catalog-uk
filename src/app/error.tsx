"use client";
import { useEffect } from "react";
import { config } from "src/config";

import Footer from "@/components/Footer";
import Header from "@/components/Header";
import { Heading } from "@/components/Heading";
import { Paragraph } from "@/components/Paragraph";
import layout from "@/layout.module.scss";
import text from "@/textContent/cs.json";

import styles from "./error.module.scss";
import { NavBarConfig } from "./layout";

const navBarConfig: NavBarConfig = {
    showProjectPage: config.SHOW_PROJECT_PAGE,
    showAboutLkodPage: config.SHOW_ABOUTLKOD_PAGE,
    showDataPrahaPage: config.SHOW_DATAPRAHA_PAGE,
};

// eslint-disable-next-line react/function-component-definition
export default function Error({ error }: { error: Error & { digest?: string }; reset: () => void }) {
    useEffect(() => {
        console.error(error);
    }, [error]);

    return (
        <html lang="cs">
            <body className={`${layout["main-layout"]}`}>
                <Header navBarConfig={navBarConfig} isErrorPage />
                <main className={layout.main} id="main">
                    <section className={` ${styles["error-page-container"]}`}>
                        <Heading tag="h1" type="h3">
                            {text.serverErrorPage.title}
                        </Heading>
                        <Paragraph>{text.serverErrorPage.subTitle}</Paragraph>
                        <Paragraph>{error.message}</Paragraph>
                    </section>
                </main>
                <Footer adminDomain="" showAdminLink="false" />
            </body>
        </html>
    );
}
