import { Metadata } from "next";
import { notFound } from "next/navigation";
import React from "react";

import { IPublisherData } from "@/(schema)/common";
import { Breadcrumb } from "@/components/Breadcrumb";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { NoResult } from "@/components/NoResult";
import OrgIconFrame from "@/components/OrganizationIconFrame";
import { PageNavigation } from "@/components/PageNavigation";
import { ResultLine } from "@/components/ResultLine";
import DataProvider from "@/context/DataProvider";
import DatasetsList from "@/datasets/(datasets)/DatasetsList";
import Loading from "@/loading";
import Meta from "@/metatags.json";
import Filters from "@/modules/(filters)/Filters";
import FilterView from "@/modules/(filters)/FilterView";
import { CatalogService } from "@/services/catalog-service";
import text from "@/textContent/cs.json";
import { stringOrArr } from "@/utils/helpers";

import styles from "./OrganizationsDatasets.module.scss";

export async function generateMetadata({ params }: params): Promise<Metadata> {
    const organizations: IPublisherData[] = await CatalogService.getAllPublishersData();
    const urlOrganization: string = params.organization;

    const org = organizations.find((org: IPublisherData) => org.slug === urlOrganization && org.count > 0);

    return {
        title: `${text.configurable.siteTitle} | ${org?.name} | ${Meta.datasetsPage.title}`,
        description: org?.description ?? Meta.appDescription,
        openGraph: {
            title: `${text.configurable.siteTitle} | ${org?.name} | ${Meta.datasetsPage.title}`,
            description: org?.description ?? "",
            images: [Meta.datasetsPage.ogImage],
        },
        twitter: {
            card: "summary_large_image",
            description: org?.description ?? "",
            images: [Meta.datasetsPage.twitterImage],
        },
    };
}

const ORG_DATASETS_PER_PAGE_COUNT = 6;

type params = {
    params: { organization: string };
    searchParams: {
        page: string;
        themeIris: string | string[];
        keywords: string | string[];
        formatIris: string | string[];
        search: string;
        perpage: string;
    };
};

const Organization = async ({ searchParams, params }: params) => {
    const organizations: IPublisherData[] = await CatalogService.getAllPublishersData();
    const urlOrganization: string = params.organization;

    const org = organizations.find((org: IPublisherData) => org.slug === urlOrganization && org.count > 0);

    if (!org) {
        notFound();
    }

    const PAGE_LABEL = org?.name;

    const { page, themeIris, keywords, formatIris, search, perpage } = searchParams;

    const filter = {
        publisherIri: org?.iri ? org.iri : "",
        themeIris: themeIris ? stringOrArr(themeIris) : [],
        keywords: keywords ? stringOrArr(keywords) : [],
        formatIris: formatIris ? stringOrArr(formatIris) : [],
    };

    const searchString = search ? search : "";

    const limit = perpage ? +perpage : ORG_DATASETS_PER_PAGE_COUNT;

    const offset = page ? (+page - limit / ORG_DATASETS_PER_PAGE_COUNT) * ORG_DATASETS_PER_PAGE_COUNT : 0;

    const { count, datasets } = await CatalogService.findDatasets({
        limit,
        offset,
        filter,
        searchString,
    });

    const publishersPromise = CatalogService.getPublishers({ filter });
    const themesPromise = CatalogService.getThemes({ filter });
    const keywordsPromise = CatalogService.getKeywords({ filter });
    const formatsPromise = CatalogService.getFormats({ filter });

    return (
        <DataProvider
            publishersPromise={publishersPromise}
            themesPromise={themesPromise}
            keywordsPromise={keywordsPromise}
            formatsPromise={formatsPromise}
        >
            <Container>
                {org ? (
                    <Breadcrumb
                        labels={[
                            { label: text.navBar.organizations, link: `/organizations` },
                            { label: org.name, link: "" },
                        ]}
                    />
                ) : (
                    false
                )}
                <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
                <div className={styles["organization-container"]}>
                    <FilterView orgPage={true} />
                    {org ? (
                        <OrgIconFrame src={org.logo ?? null} alt={org.name} name={org.name} description={org.description} />
                    ) : (
                        <Loading />
                    )}
                    <Filters label={text.filters.title} publisherIri={org?.iri} />

                    <div className={styles["organization-datasets"]}>
                        <ResultLine result={count} orgPage={true} />
                        {count === 0 && <NoResult />}
                        <DatasetsList datasets={datasets} />
                    </div>
                    <div className={styles["organization-navigation"]}>
                        <PageNavigation
                            defaultPerPage={ORG_DATASETS_PER_PAGE_COUNT}
                            page={page}
                            perpage={perpage}
                            count={count}
                        />
                    </div>
                </div>
            </Container>
        </DataProvider>
    );
};

export default Organization;
