"use client";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React, { FC } from "react";

import { Card } from "@/components/Card";
import ColorLink from "@/components/ColorLink";
import Project from "@/project.custom.json";

import styles from "./OrganizationCard.module.scss";

const errorNoImage = Project.helpers.errorNoImage;

type Props = { label: string; logoUrl: string; description: string | null; count: number; slug: string | null };

export const OrganizationCard: FC<Props> = ({ label, logoUrl, count, slug }) => {
    const router = useRouter();

    const pushOrgHandler = (count: number, slug: string) => {
        if (count > 0 && slug) {
            router.push(`/organizations/${slug}`);
        } else {
            router.push("/organizations");
        }
    };
    return (
        <Card tag={`button`} onClick={() => pushOrgHandler(count, slug ?? "")} label={label}>
            <span className={styles["organization-card"]}>
                <span
                    className={`${styles["organization-card__image-frame"]} ${
                        logoUrl ? "" : styles["organization-card__image-frame_dull"]
                    }`}
                >
                    <Image
                        src={logoUrl ? logoUrl : errorNoImage}
                        alt={label}
                        fill
                        sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw"
                    />
                </span>
                <span className={styles["organization-card__label"]}>{label}</span>
                <span className={styles["organization-card__link"]}>
                    <ColorLink
                        linkText={`${count} ${
                            count > 4 || count === 0 ? "datových sad" : count > 1 ? "datové sady" : "datová sada"
                        }`}
                        direction={`right`}
                        start
                    />
                </span>
            </span>
        </Card>
    );
};
