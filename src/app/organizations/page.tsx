import { Metadata } from "next";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { PageNavigation } from "@/components/PageNavigation";
import { ResultLine } from "@/components/ResultLine";
import { SearchForm } from "@/components/search/SearchForm";
import Meta from "@/metatags.json";
import { CatalogService } from "@/services/catalog-service";
import text from "@/textContent/cs.json";

import { OrganizationCard } from "./(components)/OrganizationCard";
import styles from "./Organizations.module.scss";

const PAGE_LABEL = Meta.organizationsPage.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.organizationsPage.title}`,
    openGraph: {
        title: `${text.configurable.siteTitle} | ${Meta.organizationsPage.title}`,
        images: [Meta.organizationsPage.ogImage],
    },
    twitter: {
        card: "summary_large_image",
        images: [Meta.organizationsPage.twitterImage],
    },
};

const ORG_PER_PAGE_COUNT = 8;

type params = {
    searchParams: {
        page: string;
        perpage: string;
    };
};

const Organizations = async ({ searchParams }: params) => {
    const organizations = await CatalogService.getAllPublishersData();
    const { page, perpage } = searchParams;

    const currentPage = page ? Number(page) : 1;
    const currentPerPage = perpage ? Number(perpage) : ORG_PER_PAGE_COUNT;
    const itemOffsetIndex = (currentPage - currentPerPage / ORG_PER_PAGE_COUNT) * ORG_PER_PAGE_COUNT;

    const pregnantOrgs = organizations.filter((org) => org.count > 0);
    const pagedOrganizations = pregnantOrgs.slice(itemOffsetIndex, itemOffsetIndex + currentPerPage);

    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <SearchForm label={text.searchString.title} fullWidth />
            <div className={styles["organizations-container"]}>
                <ResultLine result={pregnantOrgs.length} organization />
                <div className={styles["organization-list"]}>
                    {pagedOrganizations.map((org, i) => {
                        return (
                            <OrganizationCard
                                key={i}
                                label={org.name}
                                logoUrl={org.logo ?? ""}
                                description={org.description}
                                count={org.count}
                                slug={org.slug}
                            />
                        );
                    })}
                </div>
                <div className={styles.navigation}>
                    <PageNavigation
                        defaultPerPage={ORG_PER_PAGE_COUNT}
                        page={page}
                        perpage={perpage}
                        count={pregnantOrgs.length}
                    />
                </div>
            </div>
        </Container>
    );
};

export default Organizations;
