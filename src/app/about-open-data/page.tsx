import type { NextPage } from "next";
import { Metadata } from "next";
import Link from "next/link";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import { Card } from "@/components/Card";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import EducationAndLinksIcon from "@/components/icons/EducationAndLinksIcon";
import FolderIcon from "@/components/icons/FolderIcon";
import UseAndBenefitOfOD from "@/components/icons/UseAndBenefitIcon";
import WhatIsOpenDataIcon from "@/components/icons/WhatIsOpenDataIcon";
import Meta from "@/metatags.json";
import text from "@/textContent/cs.json";

import styles from "./Project.module.scss";

const PAGE_LABEL = Meta.aboutOpenDataPage.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.aboutOpenDataPage.title}`,
    openGraph: {
        title: `${text.configurable.siteTitle} | ${Meta.aboutOpenDataPage.title}`,
        images: [Meta.aboutOpenDataPage.ogImage],
    },
    twitter: {
        card: "summary_large_image",
        images: [Meta.aboutOpenDataPage.twitterImage],
    },
};

const Project: NextPage = () => {
    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <div className={styles["project-container"]}>
                <p>{text.aboutOpenDataPage.aboutCatalog}</p>
                <p>{text.aboutOpenDataPage.aboutOpenDataSubTitle1}</p>
                <div className={styles.container}>
                    <section className={styles["container-links"]}>
                        <Card tag="div" className={styles["link-card"]}>
                            <WhatIsOpenDataIcon width={24} height={28} />
                            <div className={styles["link-card-text"]}>
                                <a href="about-open-data/what-is-open-data" rel="noreferrer">
                                    <Heading tag="h3" type="h5">
                                        {text.aboutOpenDataPage.card1Title}
                                    </Heading>
                                </a>
                            </div>
                        </Card>
                        <Card tag="div" className={styles["link-card"]}>
                            <UseAndBenefitOfOD width={18} height={28} />
                            <div className={styles["link-card-text"]}>
                                <a href="about-open-data/open-data-use-and-contribution" rel="noreferrer">
                                    <Heading tag="h3" type="h5">
                                        {text.aboutOpenDataPage.card2Title}
                                    </Heading>
                                </a>
                            </div>
                        </Card>
                        <Card tag="div" className={styles["link-card"]}>
                            <FolderIcon />
                            <div className={styles["link-card-text"]}>
                                <a href="about-open-data/open-data-publish" rel="noreferrer">
                                    <Heading tag="h3" type="h5">
                                        {text.aboutOpenDataPage.card3Title}
                                    </Heading>
                                </a>
                            </div>
                        </Card>
                        <Card tag="div" className={styles["link-card"]}>
                            <div className={styles["cardtag-container"]}>
                                <EducationAndLinksIcon width={30} height={28} disabled />
                                <div className={styles["card-tag"]}>{"Připravujeme"}</div>
                            </div>
                            <div className={styles["link-card-text"]}>
                                <Heading tag="h3" type="h5" style={{ color: "#A8A8A8" }}>
                                    {text.aboutOpenDataPage.card4Title}
                                </Heading>
                            </div>
                        </Card>
                    </section>
                </div>
            </div>
        </Container>
    );
};

export default Project;
