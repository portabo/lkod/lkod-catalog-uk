import parse from "html-react-parser";
import type { NextPage } from "next";
import { Metadata } from "next";
import Image from "next/image";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import { Card } from "@/components/Card";
import ColorLink from "@/components/ColorLink";
import { ConnectedList } from "@/components/ConnectedList";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { Paragraph } from "@/components/Paragraph";
import { examples } from "@/data/opendata-examples";
import Meta from "@/metatags.json";
import text from "@/textContent/cs.json";

import styles from "./OpenDataUse.module.scss";

const PAGE_LABEL = Meta.openDataUsePage.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.openDataUsePage.title}`,
    openGraph: {
        title: `${text.configurable.siteTitle} | ${Meta.openDataUsePage.title}`,
        images: [Meta.openDataUsePage.ogImage],
    },
    twitter: {
        card: "summary_large_image",
        images: [Meta.openDataUsePage.twitterImage],
    },
};

const OpenDataUse: NextPage = () => {
    return (
        <Container>
            <Breadcrumb
                labels={[
                    { label: text.aboutOpenDataPage.aboutOpenDataTitle, link: "/about-open-data" },
                    { label: PAGE_LABEL, link: "" },
                ]}
            />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <div className={styles["project-container"]}>
                <p>{text.openDataUseAndContribution.description}</p>
                <ConnectedList>
                    <li>{parse(text.openDataUseAndContribution.paragraph1)}</li>
                    <li>{parse(text.openDataUseAndContribution.paragraph2)}</li>
                    <li>{parse(text.openDataUseAndContribution.paragraph3)}</li>
                </ConnectedList>
            </div>
            <Heading tag="h2" type="h4">
                {text.openDataUseAndContribution.subTitle}
            </Heading>
            <section className={styles["example-container"]}>
                {examples.map((example) => (
                    <Card tag="div" key={example.title} className={styles["example-card"]}>
                        <span className={styles["example-card__image-frame"]}>
                            <Image
                                src={example.img}
                                alt="example"
                                fill
                                sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw"
                            />
                        </span>
                        <span className={styles["example-card__text"]}>
                            <Heading tag="h3" type="h5">
                                {example.title}
                            </Heading>
                            <Paragraph>{example.description}</Paragraph>
                            <ColorLink
                                linkText={text.colorLink.showMore}
                                direction={`right`}
                                linkUrl={example.link}
                                blank
                                start
                            />
                        </span>
                    </Card>
                ))}
            </section>
        </Container>
    );
};

export default OpenDataUse;
