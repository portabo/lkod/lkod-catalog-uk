import parse from "html-react-parser";
import type { NextPage } from "next";
import { Metadata } from "next";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import { ConnectedList } from "@/components/ConnectedList";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Meta from "@/metatags.json";
import text from "@/textContent/cs.json";

import styles from "./WhatIsOD.module.scss";

const PAGE_LABEL = Meta.whatIsOpenDataPage.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.whatIsOpenDataPage.title}`,
    openGraph: {
        title: `${text.configurable.siteTitle} | ${Meta.whatIsOpenDataPage.title}`,
        images: [Meta.whatIsOpenDataPage.ogImage],
    },
    twitter: {
        card: "summary_large_image",
        images: [Meta.whatIsOpenDataPage.twitterImage],
    },
};

const OpenDataWhatIs: NextPage = () => {
    return (
        <Container>
            <Breadcrumb
                labels={[
                    { label: text.aboutOpenDataPage.aboutOpenDataTitle, link: "/about-open-data" },
                    { label: PAGE_LABEL, link: "" },
                ]}
            />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <div className={styles["project-container"]}>
                <p>{text.whatIsOpenDataPage.aboutOpenDataDescription}</p>
                <ConnectedList>
                    <li className={styles.heading}>{text.whatIsOpenDataPage.aboutOpenDataSubTitle}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph1)}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph2)}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph3)}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph4)}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph5)}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph6)}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph7)}</li>
                    <li>{parse(text.whatIsOpenDataPage.paragraph8)}</li>
                </ConnectedList>
                <p>{parse(text.whatIsOpenDataPage.websource)}</p>
            </div>
        </Container>
    );
};

export default OpenDataWhatIs;
