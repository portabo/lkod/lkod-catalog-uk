import parse from "html-react-parser";
import type { NextPage } from "next";
import { Metadata } from "next";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import { ConnectedList } from "@/components/ConnectedList";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Meta from "@/metatags.json";
import text from "@/textContent/cs.json";

import styles from "./PublishOD.module.scss";

const PAGE_LABEL = Meta.openDataPublishedOD.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.openDataPublishedOD.title}`,
    openGraph: {
        title: `${text.configurable.siteTitle} | ${Meta.openDataPublishedOD.title}`,
        images: [Meta.openDataPublishedOD.ogImage],
    },
    twitter: {
        card: "summary_large_image",
        images: [Meta.openDataPublishedOD.twitterImage],
    },
};

const OpenDataPublish: NextPage = () => {
    return (
        <Container>
            <Breadcrumb
                labels={[
                    { label: text.aboutOpenDataPage.aboutOpenDataTitle, link: "/about-open-data" },
                    { label: PAGE_LABEL, link: "" },
                ]}
            />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <div className={styles["project-container"]}>
                <Heading tag={`h4`}>{text.openDataPublishedOD.dataDisclosureDecision}</Heading>
                <p>{parse(text.openDataPublishedOD.aboutPublichOD)}</p>
                <p>{text.openDataPublishedOD.necessaryCorrectlyAnswer}</p>
                <ConnectedList>
                    <li>{text.openDataPublishedOD.paragraph1}</li>
                    <li>{text.openDataPublishedOD.paragraph2}</li>
                    <li>{text.openDataPublishedOD.paragraph3}</li>
                    <li>{text.openDataPublishedOD.paragraph4}</li>
                    <li>{text.openDataPublishedOD.paragraph5}</li>
                    <li>{text.openDataPublishedOD.paragraph6}</li>
                </ConnectedList>
                <Heading tag={`h4`}>{text.openDataPublishedOD.roles}</Heading>
                <p>{text.openDataPublishedOD.organizationManagement}</p>
                <p>{text.openDataPublishedOD.internalRegulation}</p>
                <Heading tag={`h4`}>{text.openDataPublishedOD.benefitAndRisk}</Heading>
                <p>{parse(text.openDataPublishedOD.benefitAndRiskDescription)}</p>
                <Heading tag={`h4`}>{text.openDataPublishedOD.plan}</Heading>
                <p>{parse(text.openDataPublishedOD.planDescription)}</p>
                <Heading tag={`h4`}>{text.openDataPublishedOD.datasetСataloguing}</Heading>
                <p>{text.openDataPublishedOD.datasetСataloguingDescription}</p>
                <ConnectedList>
                    <li>{text.openDataPublishedOD.paragraph7}</li>
                    <li>{text.openDataPublishedOD.paragraph8}</li>
                </ConnectedList>
            </div>
        </Container>
    );
};

export default OpenDataPublish;
