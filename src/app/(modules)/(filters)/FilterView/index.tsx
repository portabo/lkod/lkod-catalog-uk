"use client";
import { useRouter } from "next/navigation";

import { ButtonReset } from "@/components/ButtonReset";
import { Chip } from "@/components/Chip";
import { useFilterChips } from "@/hooks/useFilterChips";
import { useUrlFilter } from "@/hooks/useUrlFilter";
import text from "@/textContent/cs.json";
import { objectToQueryString } from "@/utils/objectToQueryString";

import styles from "./FilterView.module.scss";

type Props = {
    orgPage?: boolean;
};
const FilterView = ({ orgPage }: Props) => {
    const router = useRouter();
    const { urlFilter } = useUrlFilter();

    const { publishers, topics, keys, formats } = useFilterChips();

    const showChips = orgPage
        ? (formats && formats.length > 0) || (keys && keys.length > 0) || (topics && topics.length > 0)
        : (publishers && publishers.length > 0) ||
          (formats && formats.length > 0) ||
          (keys && keys.length > 0) ||
          (topics && topics.length > 0);

    const filterPublisher = (val: string) => {
        if (urlFilter.publisherIri === val) {
            urlFilter.publisherIri = "";
        } else {
            urlFilter.publisherIri = val;
        }

        const queryString = objectToQueryString(urlFilter);

        router.push(`?${queryString}`, { scroll: false });
    };

    const filterQuery = (key: string, val: string) => {
        urlFilter[key] = (urlFilter[key] as string[]).filter((value) => value !== val);

        const queryString = objectToQueryString(urlFilter);

        router.push(`?${queryString}`, { scroll: false });
    };

    return (
        <div className={styles["filter-view"]}>
            {showChips && (
                <>
                    Zvolené filtry:
                    <ul>
                        {!orgPage && (
                            <li>
                                {publishers && publishers.length > 0 && (
                                    <Chip
                                        label={publishers[0].label}
                                        onClick={() => {
                                            filterPublisher(publishers[0].iri);
                                        }}
                                    />
                                )}
                            </li>
                        )}
                        {topics &&
                            topics.length > 0 &&
                            topics.map((theme) => {
                                return (
                                    <li key={theme.iri}>
                                        <Chip
                                            label={theme.label}
                                            onClick={() => {
                                                filterQuery("themeIris", theme.iri);
                                            }}
                                        />
                                    </li>
                                );
                            })}
                        {keys &&
                            keys.length > 0 &&
                            keys.map((keyword) => {
                                return (
                                    <li key={keyword.label}>
                                        <Chip
                                            label={keyword.label}
                                            onClick={() => {
                                                filterQuery("keywords", keyword.label);
                                            }}
                                        />
                                    </li>
                                );
                            })}
                        {formats &&
                            formats.length > 0 &&
                            formats.map((format) => {
                                return (
                                    <li key={format.iri}>
                                        <Chip
                                            label={format.label}
                                            onClick={() => {
                                                filterQuery("formatIris", format.iri);
                                            }}
                                        />
                                    </li>
                                );
                            })}
                    </ul>
                    <ButtonReset
                        color="primary"
                        label={text.filters.cancelFilters}
                        onClick={() => {
                            router.push(`?`);
                        }}
                        end
                    />
                </>
            )}
        </div>
    );
};

export default FilterView;
