"use client";
import React, { FC, useState } from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { FilterIcon } from "@/components/icons/FilterIcon";
import { Modal } from "@/components/Modal";
import text from "@/textContent/cs.json";

import { FilterForm } from "../FilterForm";
import styles from "./Filters.module.scss";

type Props = {
    label: string;
    publisherIri?: string;
};

const Filters: FC<Props> = ({ label, publisherIri }) => {
    const [showModal, setShowModal] = useState(false);

    const modalHandler = () => {
        setShowModal(true);
        window.scrollTo({ top: 0, behavior: "smooth" });
    };

    const closeHandler = () => {
        setShowModal(false);
    };

    return (
        <div className={styles.filters} tabIndex={0}>
            <Heading tag={`h3`} type={`h4`}>
                {label}
            </Heading>
            <div className={styles["filter-list"]}>
                <Button
                    className={styles["mobile-visible"]}
                    color="outline-primary"
                    label={text.filters.filtersdatasets}
                    onClick={() => modalHandler()}
                    iconStart={<FilterIcon color="primary" width={20} height={20} />}
                />
                <Modal show={showModal} onClose={closeHandler} label={text.filters.title}>
                    <FilterForm onClose={closeHandler} publisherIri={publisherIri} />
                </Modal>
                <FilterForm className={styles["desktop-visible"]} onClose={closeHandler} publisherIri={publisherIri} />
            </div>
        </div>
    );
};

export default Filters;
