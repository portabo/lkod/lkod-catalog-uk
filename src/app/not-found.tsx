/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { useEffect } from "react";

import { CardInfo } from "@/components/CardInfo";
import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import { NoResultIcon } from "@/components/icons/NoResultIcon";
import { Paragraph } from "@/components/Paragraph";
import text from "@/textContent/cs.json";

import styles from "./not-found.module.scss";

const FourOhFour = () => {
    const router = useRouter();
    const plausible = usePlausible();

    useEffect(() => {
        plausible("404", { props: { path: document.location.pathname } });
    }, []);

    return (
        <main className={styles.main}>
            <div className={styles.container}>
                <CardInfo className={styles["four-oh-four"]}>
                    <NoResultIcon />
                    <Heading tag={`h1`} type={`h3`}>
                        {text.four04.pagedoesnotexist}
                    </Heading>
                    <Paragraph>
                        {text.four04.linkfilters} <Link href={`/`}>{text.four04.linkfilters2}</Link>.
                    </Paragraph>
                    <ColorLink linkText={text.buttons.back} onClick={() => router.back()} direction={`left`} center />
                </CardInfo>
            </div>
        </main>
    );
};

export default FourOhFour;
