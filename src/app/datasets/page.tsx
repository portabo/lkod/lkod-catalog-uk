import { Metadata } from "next";
import React from "react";
import { Suspense } from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { NoResult } from "@/components/NoResult";
import { PageNavigation } from "@/components/PageNavigation";
import { ResultLine } from "@/components/ResultLine";
import { SearchForm } from "@/components/search/SearchForm";
import Meta from "@/metatags.json";
import Filters from "@/modules/(filters)/Filters";
import FilterView from "@/modules/(filters)/FilterView";
import { CatalogService } from "@/services/catalog-service";
import text from "@/textContent/cs.json";
import { stringOrArr } from "@/utils/helpers";

import DataProvider from "../(context)/DataProvider";
import Loading from "../loading";
import DatasetsList from "./(datasets)/DatasetsList";
import styles from "./Datasets.module.scss";

type params = {
    searchParams: {
        page: string;
        publisherIri: string;
        themeIris: string | string[];
        keywords: string | string[];
        formatIris: string | string[];
        search: string;
        perpage: string;
    };
};

const DATASET_PER_PAGE_COUNT = 6;

const PAGE_LABEL = Meta.datasetsPage.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.datasetsPage.title}`,
    openGraph: {
        title: `${text.configurable.siteTitle} | ${Meta.datasetsPage.title}`,
        images: [Meta.datasetsPage.ogImage],
    },
    twitter: {
        card: "summary_large_image",
        images: [Meta.datasetsPage.twitterImage],
    },
};

const Datasets = async ({ searchParams }: params) => {
    const { page, publisherIri, themeIris, keywords, formatIris, search, perpage } = searchParams;

    const filter = {
        publisherIri: publisherIri ? publisherIri : "",
        themeIris: themeIris ? stringOrArr(themeIris) : [],
        keywords: keywords ? stringOrArr(keywords) : [],
        formatIris: formatIris ? stringOrArr(formatIris) : [],
    };

    const searchString = search ? search : "";

    const limit = perpage ? +perpage : DATASET_PER_PAGE_COUNT;

    const offset = page ? (+page - limit / DATASET_PER_PAGE_COUNT) * DATASET_PER_PAGE_COUNT : 0;

    const { count, datasets } = await CatalogService.findDatasets({
        limit,
        offset,
        filter,
        searchString,
    });

    const publishersPromise = CatalogService.getPublishers({ filter });
    const themesPromise = CatalogService.getThemes({ filter });
    const keywordsPromise = CatalogService.getKeywords({ filter });
    const formatsPromise = CatalogService.getFormats({ filter });

    return (
        <DataProvider
            publishersPromise={publishersPromise}
            themesPromise={themesPromise}
            keywordsPromise={keywordsPromise}
            formatsPromise={formatsPromise}
        >
            <Container>
                <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
                <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
                <SearchForm label={text.searchString.title} fullWidth />
                <div className={styles["datasets-container"]}>
                    <Suspense fallback={<Loading />}>
                        <Filters label={text.filters.title} />
                        <FilterView />
                        <ResultLine result={count} />
                    </Suspense>
                    <Suspense fallback={<Loading />}>
                        {+count === 0 && <NoResult />}
                        <DatasetsList datasets={datasets} />
                    </Suspense>
                    <div className={styles["datasets-navigation"]}>
                        <PageNavigation defaultPerPage={DATASET_PER_PAGE_COUNT} page={page} perpage={perpage} count={count} />
                    </div>
                </div>
            </Container>
        </DataProvider>
    );
};

export default Datasets;
