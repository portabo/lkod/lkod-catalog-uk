"use client";

import Link from "next/link";
import { usePlausible } from "next-plausible";
import React from "react";

import { Heading } from "@/components/Heading";
import ExploratoryAnalysisIcon from "@/components/icons/ExploratoryAnalysis";
import LinkIcon from "@/components/icons/LinkIcon";
import text from "@/textContent/cs.json";

import styles from "./ExploratoryAnalysis.module.scss";

type ExploratoryAnalysisProps = {
    datasetTitle: string;
};

const ExploratoryAnalysis = ({ datasetTitle }: ExploratoryAnalysisProps) => {
    const reportPath = `/reports/${datasetTitle}.html`;
    const plausible = usePlausible();

    const handleLinkClick = () => {
        plausible("ExploratoryAnalysisOpened", { props: { title: datasetTitle } });
    };
    return (
        <div className={styles.container}>
            <div className={styles["analysis__header"]}>
                <Heading tag={`h4`} type={`h4`}>
                    {text.ExploratoryAnalysis.header}
                </Heading>

                <div className={styles["analysis__header-newBadge"]}>{text.ExploratoryAnalysis.newBadge}</div>
            </div>
            <section className={styles["analysis__card"]}>
                <div className={styles["analysis__card-content"]}>
                    <ExploratoryAnalysisIcon />
                    <div className={styles["analysis__card-link"]}>
                        <LinkIcon />
                        <Link href={reportPath} target="_blank" rel="noopener noreferrer" onClick={handleLinkClick}>
                            {text.ExploratoryAnalysis.link}
                        </Link>
                    </div>
                </div>

                <span className={styles["analysis__description-header"]}>{text.ExploratoryAnalysis.descriptionHeader}</span>
                <span className={styles["analysis__description-text"]}>{text.ExploratoryAnalysis.descriptionText}</span>
            </section>
        </div>
    );
};

export default ExploratoryAnalysis;
