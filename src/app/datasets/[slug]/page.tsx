import fs from "fs";
import { Metadata, ResolvingMetadata } from "next";
import { notFound } from "next/navigation";
import path from "path";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import { Chip } from "@/components/Chip";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import IconExtSource from "@/components/IconExtSource";
import DatasetOrganizationCard from "@/datasets/(datasets)/DatasetOrganizationCard";
import { MetadataCard } from "@/datasets/(datasets)/MetadatCard";
import Meta from "@/metatags.json";
import { CatalogService } from "@/services/catalog-service";
import text from "@/textContent/cs.json";

import { DistributionFiles } from "../(distributions)/DistributionFiles";
import { DistributionServices } from "../(distributions)/DistributionServices";
import ExploratoryAnalysis from "./(ExploratoryAnalysis)";
import styles from "./DatasetDetail.module.scss";

type params = {
    params: { slug: string };
    searchParams: {
        page: string;
        themeIris: string | string[];
        keywords: string | string[];
        formatIris: string | string[];
        search: string;
        perpage: string;
    };
};

export async function generateMetadata({ params }: params, parent: ResolvingMetadata): Promise<Metadata> {
    const iri = decodeURIComponent(params.slug);
    const metaData = await CatalogService.getDataset(iri);

    const previousImages = (await parent).openGraph?.images || [];

    return {
        title: `${Meta.appTitle} ${metaData?.title ? `| ${metaData.title}` : ``}`,
        description: metaData?.description || Meta.appDescription,
        keywords: metaData?.keywords || Meta.keywords,
        openGraph: {
            title: metaData?.title,
            description: metaData?.description || Meta.appDescription,
            images: [Meta.datasetsPage.ogImage || Meta.ogDefault.ogImage, ...previousImages],
        },
        twitter: {
            card: "summary_large_image",
            description: metaData?.description || Meta.appDescription,
            images: [Meta.datasetsPage.twitterImage || Meta.ogDefault.twitterImage, ...previousImages],
        },
    };
}

const DatasetPage = async ({ params }: params) => {
    const iri = decodeURIComponent(params.slug);
    const data = await CatalogService.getDataset(iri);

    if (!data) {
        notFound();
    }

    const distributionFilesPromise = CatalogService.getDistributionFiles(iri);

    const distributionServicesPromise = CatalogService.getDistributionServices(iri);

    const reportPath = path.join(process.cwd(), "public", "reports", `${data.title}.html`);
    const reportExists = fs.existsSync(reportPath);
    return (
        <Container>
            <Breadcrumb
                labels={[
                    { label: text.navBar.datasets, link: `/datasets` },
                    { label: data?.title, link: "" },
                ]}
            />
            <Heading tag={`h1`} type={`h2`}>
                {data?.title}
            </Heading>
            <p className={styles.description}>{data.description}</p>
            <section className={styles["title-container"]}>
                {data.themes.map((theme, i) => {
                    return (
                        <div key={i} className={styles["title-icon"]}>
                            {theme.image ? <IconExtSource src={theme.image} alt={theme.title} /> : false}
                            <span className={i > 0 ? styles["dashed-span"] : ""}>{theme.title}</span>
                            {data.publisher ? <span>{data.publisher.title}</span> : false}
                        </div>
                    );
                })}
            </section>
            <div className={data.publisher ? styles["detail-container"] : styles["detail-container-noorg"]}>
                {data.publisher ? (
                    <DatasetOrganizationCard
                        label={data.publisher.title}
                        logoUrl={data.publisher.logo ?? ""}
                        organizationUrl={data.publisher.iri}
                    />
                ) : (
                    false
                )}
                {data.distributions.files.length > 0 ? (
                    <DistributionFiles distributionFilesPromise={distributionFilesPromise} datasetTitle={data.title} />
                ) : (
                    false
                )}
                {data.distributions.services.length > 0 ? (
                    <DistributionServices distributionServicesPromise={distributionServicesPromise} />
                ) : (
                    false
                )}
                <section className={styles.metadata}>
                    <Heading tag={`h4`}>{text.heading.metadata}</Heading>
                    <MetadataCard label={text.metadataCard.keywords}>
                        <div className={styles["metadata__keywords"]}>
                            {data.keywords.map((kword, i) => {
                                return <Chip key={i} label={kword} />;
                            })}
                        </div>
                    </MetadataCard>
                    {data.documentation && (
                        <MetadataCard label={text.metadataCard.documentlink}>
                            <a
                                href={`${decodeURIComponent(data.documentation ?? "")}`}
                                className={styles["metadata__link"]}
                                target="_blank"
                                rel="noreferrer"
                            >
                                {decodeURIComponent(data.documentation ?? "")}
                            </a>
                        </MetadataCard>
                    )}
                    {data.specification && (
                        <MetadataCard label={text.metadataCard.specificationlink}>
                            <a
                                href={`${decodeURIComponent(data.specification ?? "")}`}
                                className={styles["metadata__link"]}
                                target="_blank"
                                rel="noreferrer"
                            >
                                {decodeURIComponent(data.specification ?? "")}
                            </a>
                        </MetadataCard>
                    )}
                    {data.contactPoint && (
                        <MetadataCard label={text.metadataCard.contacts}>
                            <a href={data.contactPoint.email}>{data.contactPoint.name}</a>
                        </MetadataCard>
                    )}
                    {data.themes.length > 0 && (
                        <MetadataCard label={text.metadataCard.theme}>
                            <div className={styles["metadata__themes"]}>
                                {data.themes.map((theme, i) => {
                                    return (
                                        <div key={i} className={styles["metadata__theme"]}>
                                            {theme.image ? <IconExtSource src={theme.image} alt={theme.title} /> : false}
                                            <p>{theme.title}</p>
                                        </div>
                                    );
                                })}
                            </div>
                        </MetadataCard>
                    )}
                    {data.accrualPeriodicity && (
                        <MetadataCard label={text.metadataCard.updatefrequency}>
                            <p>{data.accrualPeriodicity}</p>
                        </MetadataCard>
                    )}
                    {data.temporalFrom && data.temporalTo && (
                        <MetadataCard label={text.metadataCard.timeCoverage}>
                            <div className={styles["metadata__temporal-line"]}>
                                <p>{data.temporalFrom}</p>
                                <p>{data.temporalTo}</p>
                            </div>
                        </MetadataCard>
                    )}
                    {data.temporalResolution && (
                        <MetadataCard label={text.metadataCard.accruals}>
                            <p>{data.temporalResolution}</p>
                        </MetadataCard>
                    )}
                    {data.spatialResolutionInMeters && (
                        <MetadataCard label={text.metadataCard.resolutioninmeters}>
                            <p>{data.spatialResolutionInMeters}</p>
                        </MetadataCard>
                    )}
                    {data.spatial && (
                        <MetadataCard label={text.metadataCard.geographicarea}>
                            <div className={styles["metadata__links"]}>
                                {data.spatial.map((link, i) => {
                                    return (
                                        <a
                                            key={i}
                                            href={link}
                                            className={styles["metadata__link"]}
                                            target="_blank"
                                            rel="noreferrer"
                                        >
                                            {link}
                                        </a>
                                    );
                                })}
                            </div>
                        </MetadataCard>
                    )}
                    {data.eurovocThemes && data.eurovocThemes.length > 0 && (
                        <MetadataCard label={text.metadataCard.classification}>
                            <div className={styles["metadata__links"]}>
                                {data.eurovocThemes.map((link, i) => {
                                    return (
                                        <a
                                            key={i}
                                            href={link}
                                            className={styles["metadata__link"]}
                                            target="_blank"
                                            rel="noreferrer"
                                        >
                                            {link}
                                        </a>
                                    );
                                })}
                            </div>
                        </MetadataCard>
                    )}
                    {reportExists && <ExploratoryAnalysis datasetTitle={data.title} />}
                </section>
            </div>
        </Container>
    );
};

export default DatasetPage;
