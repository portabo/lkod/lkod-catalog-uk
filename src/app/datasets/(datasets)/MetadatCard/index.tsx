import React, { ReactNode } from "react";

import styles from "./MetadataCard.module.scss";

type Props = { label: string; children: ReactNode };

export const MetadataCard = ({ label, children }: Props) => {
    return (
        <section className={styles["metadata-card"]}>
            <span className={styles["metadata-card__label"]}>{label}</span>
            {children}
        </section>
    );
};
