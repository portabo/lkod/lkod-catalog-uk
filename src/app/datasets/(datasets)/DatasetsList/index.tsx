"use client";
import React from "react";

import { DatasetsItem } from "@/schema/dataset";

import DatasetCard from "../DatasetCard";
import styles from "./DatasetsList.module.scss";

type Props = {
    datasets: DatasetsItem[];
};

const DatasetsList = ({ datasets }: Props) => {
    return (
        datasets.length > 0 && (
            <div className={styles["datasets-list"]}>
                {datasets.map((ds: DatasetsItem) => {
                    return (
                        <DatasetCard
                            key={ds.iri}
                            iri={ds.iri}
                            title={ds.title}
                            description={ds.description}
                            publisher={ds.publisher}
                            formats={ds.formats}
                            themeNames={ds.themeNames}
                            themeImage={ds.themeImage}
                        />
                    );
                })}
            </div>
        )
    );
};

export default DatasetsList;
