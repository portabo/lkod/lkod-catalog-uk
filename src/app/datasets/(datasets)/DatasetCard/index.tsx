import React from "react";

import { DatasetsItem } from "@/(schema)/dataset";
import { Card } from "@/components/Card";
import { Heading } from "@/components/Heading";
import IconExtSource from "@/components/IconExtSource";
import generalThemeImage from "@/images/topic_icons/general.svg";

import styles from "./DatasetCard.module.scss";

const DatasetCard = ({ description, iri, themeNames, publisher, formats, themeImage, title }: DatasetsItem) => {
    const formatString = formats?.join(" | ");
    const themeString = themeNames?.join(" | ");
    const encodedIri = encodeURIComponent(iri);

    return (
        <Card tag={`a`} href={`/datasets/${encodedIri}`} aria-label={title}>
            <article className={styles["dataset-card"]}>
                <p className={styles["dataset-card__top-line"]}>{`${themeString} ${publisher ? "- " + publisher : ""}`}</p>
                <div className={styles["dataset-card__heading"]}>
                    <Heading tag={`h4`} type={`h5`}>
                        {title}
                    </Heading>
                    <IconExtSource src={(themeImage as string) || generalThemeImage} alt={title} />
                </div>
                <p className={styles["dataset-card__description"]}>{description}</p>
                <p className={styles["dataset-card__formats"]}>{formatString}</p>
            </article>
        </Card>
    );
};

export default DatasetCard;
