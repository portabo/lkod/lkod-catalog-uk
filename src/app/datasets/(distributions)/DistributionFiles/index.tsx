"use client";
import { usePlausible } from "next-plausible";
import React, { use, useState } from "react";

import { Distribution, DistributionLicence } from "@/(schema)";
import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { FileCleanIcon } from "@/components/icons/FileCleanIcon";
import { InfoIcon } from "@/components/icons/InfoIcon";
import { DistributionDialog } from "@/datasets/(distributions)/DistributionDialog";
import text from "@/textContent/cs.json";

import styles from "./DistributionFiles.module.scss";

export type DistributionFile = Distribution & {
    licence: DistributionLicence;
};

export const DistributionFiles = ({
    distributionFilesPromise,
    datasetTitle,
}: {
    distributionFilesPromise: Promise<DistributionFile[] | undefined>;
    datasetTitle: string;
}) => {
    const plausible = usePlausible();
    const distributionFiles = use(distributionFilesPromise);

    const [distributionFile, setDistributionFile] = useState<DistributionFile | null>(null);

    const downloadFile = (url: string) => {
        window.location.href = url;
    };

    return (
        distributionFiles && (
            <>
                <section className={styles["dataset-files"]}>
                    <Heading tag={`h4`}>{text.distribution.filesHeading}</Heading>
                    {distributionFiles.map((file, i) => {
                        return (
                            <div className={styles["file-card"]} key={i}>
                                <div>
                                    <div className={styles["file-card__row"]}>
                                        <div className={styles["file-card__icon"]}>
                                            <FileCleanIcon width={"2rem"} />
                                        </div>
                                        <div className={styles["file-card__heading"]}>
                                            {file.title ? (
                                                <Heading tag={`h5`} type={`h6`} className={styles["file-card__label"]}>
                                                    {file.title}
                                                </Heading>
                                            ) : (
                                                false
                                            )}
                                            <button
                                                className={styles["file-card__info"]}
                                                onClick={() => setDistributionFile(file)}
                                                type="button"
                                            >
                                                <InfoIcon color="secondary" width="1.4rem" height="1.4rem" />
                                                <span className={styles["file-card__info-text"]}>
                                                    {text.distribution.fileInfo}
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                    <p className={styles["file-type"]}>
                                        {file.format.title ?? text.distribution.fileTypeUnknown}
                                    </p>
                                </div>
                                <div className={styles["file-card__col"]}>
                                    <Button
                                        className={styles["file-card__download"]}
                                        label={text.buttons.download}
                                        color="secondary"
                                        type="button"
                                        onClick={() => {
                                            downloadFile(file.downloadUrl);
                                            plausible("trackFileDownloads: DownloadFile", {
                                                props: {
                                                    fileName: file.title,
                                                    fileFormat: file.format.title,
                                                    dataset: datasetTitle,
                                                    file:
                                                        "Datový soubor: " +
                                                        file.title +
                                                        ", format: " +
                                                        file.format.title +
                                                        ",  Datová sada: " +
                                                        datasetTitle,
                                                },
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                        );
                    })}
                </section>
                {distributionFile && (
                    <DistributionDialog
                        closeDialog={() => setDistributionFile(null)}
                        distributionFile={distributionFile}
                        isOpen={!!distributionFile}
                    />
                )}
            </>
        )
    );
};
