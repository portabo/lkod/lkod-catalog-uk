import Image from "next/image";
import Link from "next/link";
import React from "react";

import styles from "./LicenceCard.module.scss";

type Props = {
    source: {
        text: string;
        icon: string;
        link: string | null | undefined;
    };
};

const LicenceCard = ({ source }: Props) => {
    return (
        <div className={styles["licence-card"]}>
            <div className={styles["licence-card__image"]}>
                <Image src={source.icon} alt={source.icon} />
            </div>
            <Link href={source.link ?? ""}>
                <p>{source.text}</p>
            </Link>
        </div>
    );
};

export default LicenceCard;
