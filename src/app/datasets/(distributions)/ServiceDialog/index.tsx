import React, { FC, useEffect } from "react";

import { DistributionService } from "@/(schema)/distribution";
import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { Modal } from "@/components/Modal";
import text from "@/textContent/cs.json";

import styles from "../DistributionDialog/DistributionDialog.module.scss";
import { MetadataModalCard } from "../MetadataModalCard";

const ServiceDialog: FC<{ isOpen: boolean; closeDialog: () => void; distributionService: DistributionService }> = ({
    isOpen,
    closeDialog,
    distributionService,
}) => {
    useEffect(() => {
        window.scrollTo({ top: 10, behavior: "smooth" });
    }, []);

    return (
        <Modal show={isOpen} onClose={closeDialog} label={text.distribution.serviceInfoHeading}>
            {distributionService && (
                <section className={styles["modal-card__data"]}>
                    <Heading tag={`h5`}>{text.heading.metadata}</Heading>
                    <MetadataModalCard
                        label={text.metadataModalCard.datasetdistributionname}
                        data={distributionService.title ?? ""}
                    />
                    {distributionService.endpointURL && (
                        <MetadataModalCard
                            link
                            label={text.metadataModalCard.accesspoint}
                            data={distributionService.endpointURL}
                        />
                    )}
                    {distributionService.endpointDescription && (
                        <MetadataModalCard
                            link
                            label={text.metadataModalCard.accesspointDescription}
                            data={distributionService.endpointDescription}
                        />
                    )}
                    {distributionService.conformsTo && (
                        <MetadataModalCard
                            link
                            label={text.metadataModalCard.linktospecification}
                            data={distributionService.conformsTo}
                        />
                    )}
                </section>
            )}
            <div className={styles["modal-card__controls"]}>
                <Button label={text.buttons.close} color="primary" onClick={closeDialog} />
            </div>
        </Modal>
    );
};

export default ServiceDialog;
