import Link from "next/link";
import React from "react";

import styles from "./MetadataModalCard.module.scss";

type Props = { label: string; data: string; link?: boolean };

export const MetadataModalCard = ({ label, data, link }: Props) => {
    return (
        <div className={styles["metadata-modalcard"]}>
            <p className={styles["metadata-modalcard__title"]}>{label}</p>
            {link ? (
                <Link href={data} target="_blank" rel="noreferrer">
                    {data}
                </Link>
            ) : (
                <p>{data}</p>
            )}
        </div>
    );
};
