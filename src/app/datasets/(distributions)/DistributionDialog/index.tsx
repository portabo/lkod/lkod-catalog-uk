import React from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { Modal } from "@/components/Modal";
import text from "@/textContent/cs.json";
import { getCopyright, getCopyrightedDB, getPII, getSpecialDB } from "@/utils/distributionLicenceGetters";

import { DistributionFile } from "../DistributionFiles";
import LicenceCard from "../LicenceCard/LicenceCard";
import { MetadataModalCard } from "../MetadataModalCard";
import styles from "./DistributionDialog.module.scss";

type DistributionDialogProps = { isOpen: boolean; closeDialog: () => void; distributionFile: DistributionFile };

export const DistributionDialog = ({ isOpen, closeDialog, distributionFile }: DistributionDialogProps) => {
    const distributionLicence = distributionFile.licence;

    const copyrighted = getCopyright(distributionLicence?.["autorské-dílo"]);
    const copyrightedDB = getCopyrightedDB(distributionLicence?.["databáze-jako-autorské-dílo"]);
    const specialDB = getSpecialDB(distributionLicence?.["databáze-chráněná-zvláštními-právy"]);
    const pii = getPII(distributionLicence?.["osobní-údaje"]);

    return (
        <Modal
            show={isOpen}
            onClose={closeDialog}
            label={text.distribution.fileInfoHeading}
            titleData={distributionFile.title ?? ""}
        >
            {distributionLicence && (
                <section className={styles["modal-card__data"]}>
                    <Heading tag={`h5`}>{text.heading.termsofuse}</Heading>
                    <div className={styles["modal-card__licences"]}>
                        <div className={styles["modal-card__licences_grouped"]}>
                            <LicenceCard source={copyrighted} />
                            <LicenceCard source={copyrightedDB} />
                        </div>
                        <div className={styles["modal-card__licences_grouped"]}>
                            <LicenceCard source={specialDB} />
                            <LicenceCard source={pii} />
                        </div>
                    </div>
                </section>
            )}
            {distributionFile && (
                <section className={styles["modal-card__data"]}>
                    <Heading tag={`h5`}>{text.heading.metadata}</Heading>
                    <MetadataModalCard
                        label={text.metadataModalCard.datasetdistributionname}
                        data={distributionFile.title ?? ""}
                    />
                    {distributionFile.downloadUrl && (
                        <MetadataModalCard link label={text.metadataModalCard.downloadlink} data={distributionFile.downloadUrl} />
                    )}
                    {distributionFile.accessUrl && (
                        <MetadataModalCard link label={text.metadataModalCard.url} data={distributionFile.accessUrl} />
                    )}
                    {distributionFile.format.title && (
                        <MetadataModalCard label={text.metadataModalCard.format} data={distributionFile.format.title} />
                    )}
                    {distributionFile.mediaType && (
                        <MetadataModalCard label={text.metadataModalCard.mediatype} data={distributionFile.mediaType} />
                    )}
                    {distributionFile.conformsTo && (
                        <MetadataModalCard
                            link
                            label={text.metadataModalCard.linktoschematic}
                            data={distributionFile.conformsTo}
                        />
                    )}
                    {distributionFile.compressFormat && (
                        <MetadataModalCard
                            label={text.metadataModalCard.mediatypecompression}
                            data={distributionFile.compressFormat}
                        />
                    )}
                    {distributionFile.packageFormat && (
                        <MetadataModalCard
                            label={text.metadataModalCard.mediatypepackaging}
                            data={distributionFile.packageFormat}
                        />
                    )}
                </section>
            )}
            <div className={styles["modal-card__controls"]}>
                <Button label={text.buttons.close} color="primary" onClick={closeDialog} />
            </div>
        </Modal>
    );
};
