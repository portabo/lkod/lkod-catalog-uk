"use client";
import React, { use, useState } from "react";

import { DistributionService } from "@/(schema)";
import { Heading } from "@/components/Heading";
import { FileCleanIcon } from "@/components/icons/FileCleanIcon";
import { InfoIcon } from "@/components/icons/InfoIcon";
import ServiceDialog from "@/datasets/(distributions)/ServiceDialog";
import text from "@/textContent/cs.json";

import styles from "./DistributionServices.module.scss";

export const DistributionServices = ({
    distributionServicesPromise,
}: {
    distributionServicesPromise: Promise<DistributionService[] | undefined>;
}) => {
    const distributionServices = use(distributionServicesPromise);
    const [distributionService, setDistributionService] = useState<DistributionService | null>(null);

    return (
        distributionServices && (
            <>
                <section className={`${styles["dataset-files"]} ${styles["dataset-services"]}`}>
                    <Heading tag={`h4`}>{text.heading.dataservices}</Heading>
                    {distributionServices.map((service, i) => {
                        return (
                            <div className={styles["file-card"]} key={i}>
                                <div className={styles["file-card__row"]}>
                                    <FileCleanIcon />
                                    <div className={styles["file-card__heading"]}>
                                        <Heading tag={`h5`} type={`h6`} className={styles["file-card__label"]}>
                                            {service.title}
                                        </Heading>
                                        <button
                                            className={styles["file-card__info"]}
                                            onClick={() => setDistributionService(service)}
                                        >
                                            <InfoIcon color="secondary" width="1.4rem" height="1.4rem" />
                                            <p>{text.distribution.serviceInfo}</p>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </section>
                {distributionService && (
                    <ServiceDialog
                        closeDialog={() => setDistributionService(null)}
                        distributionService={distributionService}
                        isOpen={!!distributionService}
                    />
                )}
            </>
        )
    );
};
