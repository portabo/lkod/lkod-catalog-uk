import { config } from "src/config";

import { JsonService } from "@/services/json-service";
import { SparqlService } from "@/services/sparql-service";

const isSparql = config.DATA_SOURCE === "sparql";

export const CatalogService = isSparql ? new SparqlService() : new JsonService();
