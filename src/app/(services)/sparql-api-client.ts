/* eslint-disable @typescript-eslint/no-explicit-any */
import { config } from "src/config";

import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/utils/getErrorMessage";

export interface SparqlResult<T extends { [key: string]: any }> {
    head: { link: string[]; vars: string[] };

    results: {
        distinct: boolean;
        ordered: boolean;
        bindings: {
            [K in keyof T]: {
                type: string;
                value: T[K];
                datatype?: string;
                "xml:lang"?: string;
            };
        }[];
    };
}

export interface DocumentFields {
    field: string;
    value: any;
}

interface ResponseEntity<T> {
    status: boolean;
    payload?: Promise<T> | undefined;
    error?: string;
}

const logger = getLogger("sparql-api-client");

const headers = new Headers({
    Accept: "application/json",
});

export class SparqlApiClient {
    async query<T>(query: string): Promise<T[]> {
        const logData = {
            class: "SparqlApiClient",
            method: "query",
        };

        try {
            const response = await this.rawQuery<any>(query);

            if (response.status && response.payload) {
                const data = await response.payload;
                logger.debug({
                    ...logData,
                    message: `Response status: ${response.status}`,
                });
                return data.results.bindings.map((doc: any) => {
                    const result: { [key: string]: any } = {};
                    for (const key in doc) {
                        result[key] = doc[key].value;
                    }
                    return result;
                });
            } else {
                logger.error({
                    ...logData,
                    message: `Error querying server: ${response.error}, returning empty array`,
                });
                return [] as T[];
            }
        } catch (error) {
            logger.error({
                ...logData,
                message: `Error querying server: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error querying server: ${getErrorMessage(error)}`);
        }
    }

    async rawQuery<T>(query: string): Promise<ResponseEntity<T>> {
        const queryParams = new URLSearchParams({ query });
        const url = `${config.ENDPOINT}?${queryParams.toString()}`;
        const { signal } = new AbortController();

        const logData = {
            class: "SparqlApiClient",
            method: "rawQuery",
            requestUrl: url,
        };

        try {
            const response = await fetch(url, {
                method: "GET",
                keepalive: true,
                headers: headers,
                next: { revalidate: 300 },
                signal,
            });

            if (response.ok) {
                logger.debug({ ...logData, message: `status: ${response.status}` });
                return {
                    status: true,
                    payload: response.json() as Promise<T>,
                };
            } else {
                logger.error({
                    ...logData,
                    message: `Response status: ${response.status}, response statusText: ${response.statusText}`,
                });
                return {
                    status: false,
                    payload: Promise.resolve<T>({} as T),
                    error: response.statusText,
                };
            }
        } catch (error: any) {
            logger.error({
                ...logData,
                error: `error: ${getErrorMessage(error)}, cause: ${error.cause || ""}`,
            });
            return {
                status: false,
                error: getErrorMessage(error),
            };
        }
    }
}

const sparql = new SparqlApiClient();
export { sparql };
