import { config } from "src/config";

import {
    Dataset,
    DatasetJson,
    DatasetJsonItem,
    DatasetsItem,
    IListFormat,
    IListKeyword,
    IListPublisher,
    IListTheme,
    IPublisherData,
    IThemeData,
} from "@/(schema)";
import { DistributionFile } from "@/datasets/(distributions)/DistributionFiles";
import { getLogger } from "@/logging/logger";
import { DistributionService } from "@/schema/distribution";
import { JsonApiClient } from "@/services/json-api-client";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { joinUrlPaths } from "@/utils/joinUrlPaths";
import { lastUrlPart } from "@/utils/lastUrlPart";
import { transformDataset } from "@/utils/transformations/transformDataset";
import { transformDatasets } from "@/utils/transformations/transformDatasets";
import { transformDatasetToDistributionFiles } from "@/utils/transformations/transformDatasetToDistributionFiles";
import { transformDatasetToDistributionServices } from "@/utils/transformations/transformDatasetToDistributionServices";
import { transformThemes } from "@/utils/transformations/transformThemes";

import { DatasetQueryOptions } from "./sparql-service";

const logger = getLogger("JsonService");

export class JsonService {
    logHeaders = {
        provider: "server",
        class: "JsonService",
    };

    async findDatasets(options: DatasetQueryOptions): Promise<{ count: number; datasets: DatasetsItem[] }> {
        const logData = {
            ...this.logHeaders,
            method: "findDatasets",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/datasets"),
        };
        try {
            const response = await JsonApiClient.get<{ total_count: number; dataset_basic_info: DatasetJsonItem[] }>(
                "public/datasets",
                options
            );
            if (
                response.status !== 200 ||
                !response.payload ||
                !response.payload.total_count ||
                !response.payload.dataset_basic_info
            ) {
                logger.error({
                    ...logData,
                    message:
                        `Error during findDatasets: ${response.error}, response status: ${response.status}, ` +
                        `returning empty array and 0 count`,
                });
                return { count: 0, datasets: [] };
            }
            logger.debug({
                ...logData,
                message: `total_count: ${response.payload.total_count} | options: ${JSON.stringify(options)}`,
            });
            return { count: response.payload.total_count, datasets: transformDatasets(response.payload.dataset_basic_info) };
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during findDatasets: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during findDatasets: ${getErrorMessage(error)}`);
        }
    }

    async getDataset(iri: string): Promise<Dataset | null> {
        const logData = {
            ...this.logHeaders,
            method: "getDataset",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/datasets", lastUrlPart(iri)),
        };
        try {
            const response = await JsonApiClient.get<DatasetJson>(joinUrlPaths("public/datasets", lastUrlPart(iri)));
            if (response.status === 200 && response.payload) {
                logger.debug({
                    ...logData,
                    message: `Dataset found: ${response.payload.title}`,
                });
                return transformDataset(response.payload);
            }
            logger.error({
                ...logData,
                message: `Dataset not found: ${response.error}, returning null`,
            });
            return null;
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getDataset: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getDataset: ${getErrorMessage(error)}`);
        }
    }

    async getDistributionData<T>(iri: string, transform: (payload: DatasetJson) => T[]): Promise<T[] | undefined> {
        const logData = {
            ...this.logHeaders,
            method: "getDistributionData",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/datasets", lastUrlPart(iri)),
        };
        try {
            const response = await JsonApiClient.get<DatasetJson>(joinUrlPaths("public/datasets", lastUrlPart(iri)));
            if (response.status === 200 && response.payload) {
                logger.debug({
                    ...logData,
                    message: `Distribution data found: ${response.payload.title}`,
                });
                return transform(response.payload);
            }
            logger.error({
                ...logData,
                message: `Distribution data not found: ${response.error}, returning undefined`,
            });
            return undefined;
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getDistributionData: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getDistributionData: ${getErrorMessage(error)}`);
        }
    }

    async getDistributionFiles(iri: string): Promise<DistributionFile[] | undefined> {
        return this.getDistributionData(iri, transformDatasetToDistributionFiles);
    }

    async getDistributionServices(iri: string): Promise<DistributionService[] | undefined> {
        return this.getDistributionData(iri, transformDatasetToDistributionServices);
    }

    async getAllPublishersData(): Promise<IPublisherData[]> {
        const logData = {
            ...this.logHeaders,
            method: "getAllPublishersData",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/organizations"),
        };
        try {
            const response = await JsonApiClient.get<IPublisherData[]>("public/organizations");
            if (response.status === 200 && response.payload) {
                logger.debug({
                    ...logData,
                    message: `Publishers found: ${response.payload.length}`,
                });
                return response.payload;
            }
            logger.error({
                ...logData,
                message: `Publishers not found: ${response.error}, returning empty array`,
            });
            return [];
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getAllPublishersData: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getAllPublishersData: ${getErrorMessage(error)}`);
        }
    }

    async getAllThemesData(): Promise<IThemeData[]> {
        const logData = {
            ...this.logHeaders,
            method: "getAllThemesData",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/themes"),
        };
        try {
            const response = await JsonApiClient.get<IListTheme[]>("public/themes");
            if (response.status === 200 && response.payload) {
                logger.debug({
                    ...logData,
                    message: `Themes found: ${response.payload.length}`,
                });
                return transformThemes(response.payload);
            }
            logger.error({
                ...logData,
                message: `Themes not found: ${response.error}, returning empty array`,
            });
            return [];
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getAllThemesData: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getAllThemesData: ${getErrorMessage(error)}`);
        }
    }

    async getPublishers(options: DatasetQueryOptions): Promise<IListPublisher[]> {
        const logData = {
            ...this.logHeaders,
            method: "getPublishers",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/datasets"),
        };
        try {
            const response = await JsonApiClient.get<{ publisher_count: IListPublisher[] }>("public/datasets", options);
            if (response.status === 200 && response.payload?.publisher_count) {
                logger.debug({
                    ...logData,
                    message: `Publishers found: ${response.payload.publisher_count?.length}`,
                });
                return response.payload.publisher_count;
            }
            logger.error({
                ...logData,
                message: `Publishers not found: ${response.error}, returning empty array`,
            });
            return [];
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getPublishers: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getPublishers: ${getErrorMessage(error)}`);
        }
    }

    async getThemes(options: DatasetQueryOptions): Promise<IListTheme[]> {
        const logData = {
            ...this.logHeaders,
            method: "getThemes",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/datasets"),
        };
        try {
            const response = await JsonApiClient.get<{ theme_count: IListTheme[] }>("public/datasets", options);
            if (response.status === 200 && response.payload?.theme_count) {
                logger.debug({
                    ...logData,
                    message: `Themes found: ${response.payload.theme_count?.length}`,
                });
                return response.payload.theme_count;
            }
            logger.error({
                ...logData,
                message: `Themes not found: ${response.error}, returning empty array`,
            });
            return [];
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getThemes: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getThemes: ${getErrorMessage(error)}`);
        }
    }

    async getKeywords(options: DatasetQueryOptions): Promise<IListKeyword[]> {
        const logData = {
            ...this.logHeaders,
            method: "getKeywords",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/datasets"),
        };
        try {
            const response = await JsonApiClient.get<{ keyword_count: IListKeyword[] }>("public/datasets", options);
            if (response.status === 200 && response.payload?.keyword_count) {
                logger.debug({
                    ...logData,
                    message: `Keywords found: ${response.payload.keyword_count?.length}`,
                });
                return response.payload.keyword_count;
            }
            logger.error({
                ...logData,
                message: `Keywords not found: ${response.error}, returning empty array`,
            });
            return [];
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getKeywords: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getKeywords: ${getErrorMessage(error)}`);
        }
    }

    async getFormats(options: DatasetQueryOptions): Promise<IListFormat[]> {
        const logData = {
            ...this.logHeaders,
            method: "getFormats",
            requestUrl: joinUrlPaths(config.ENDPOINT, "public/datasets"),
        };
        try {
            const response = await JsonApiClient.get<{ format_count: IListFormat[] }>("public/datasets", options);
            if (response.status === 200 && response.payload?.format_count) {
                logger.debug({
                    ...logData,
                    message: `Formats found: ${response.payload.format_count?.length}`,
                });
                return response.payload.format_count;
            }
            logger.error({
                ...logData,
                message: `Formats not found: ${response.error}, returning empty array`,
            });
            return [];
        } catch (error) {
            logger.error({
                ...logData,
                error: `Error during getFormats: ${getErrorMessage(error)}`,
            });
            throw new Error(`Error during getFormats: ${getErrorMessage(error)}`);
        }
    }
}
