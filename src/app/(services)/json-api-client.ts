import qs from "qs";
import { config } from "src/config";

import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { joinUrlPaths } from "@/utils/joinUrlPaths";
import { lastUrlPart } from "@/utils/lastUrlPart";

import { DatasetQueryOptions } from "./sparql-service";

const API_BASE = config.ENDPOINT;

const logger = getLogger("json-api-client");

type ResponseEntity<T> = {
    status: number;
    payload?: T;
    error?: string;
};

export class JsonApiClient {
    static async get<T>(path: string, params?: DatasetQueryOptions): Promise<ResponseEntity<T>> {
        const query = qs.stringify(
            {
                ...(params?.limit ? { limit: params.limit } : {}),
                ...(params?.offset ? { offset: params.offset } : {}),
                ...(params?.filter?.themeIris ? { theme_iris: params.filter.themeIris } : {}),
                ...(params?.filter?.publisherIri ? { publisher_slug: lastUrlPart(params.filter.publisherIri) } : {}),
                ...(params?.filter?.keywords ? { keywords: params.filter.keywords } : {}),
                ...(params?.filter?.formatIris ? { format_iris: params.filter.formatIris } : {}),
                ...(params?.searchString ? { search_text: params.searchString } : {}),
            },
            { encodeValuesOnly: true }
        );

        const headers = new Headers();
        headers.append("Content-Type", "application/json; charset=utf-8");
        headers.append("Accept-Encoding", "gzip");
        headers.append("User-Agent", "lkodCatFe");

        const requestUrl = `${path}${query !== "" ? `?${query}` : ""}`;

        const logData = {
            provider: "server",
            class: "JsonApiClient",
            method: "request - get",
            requestUrl: joinUrlPaths(API_BASE, requestUrl),
            headers: {
                "Content-Type": headers.get("Content-Type"),
                "Accept-Encoding": headers.get("Accept-Encoding"),
                "User-Agent": headers.get("User-Agent"),
            },
        };

        return fetch(joinUrlPaths(API_BASE, requestUrl), {
            method: "GET",
            headers,
            next: { revalidate: 300 },
        })
            .then(async (response) => {
                const message =
                    `Response status: ${response.status}, ` +
                    `response statusText: ${response.statusText !== "" ? response.statusText : "empty"}`;
                if (response.ok) {
                    const payload = await response.json();
                    logger.debug({
                        ...logData,
                        message: `${message}, totalCount: ${payload.total_count}`,
                    });
                    return {
                        status: response.status,
                        payload,
                    };
                } else {
                    logger.error({
                        ...logData,
                        message,
                    });
                    return {
                        status: response.status,
                        error: response.statusText,
                    };
                }
            })
            .catch((error) => {
                logger.error({
                    ...logData,
                    error: getErrorMessage(error),
                });
                throw new Error(`Failed to fetch the data: ${getErrorMessage(error)}`);
            });
    }
}
