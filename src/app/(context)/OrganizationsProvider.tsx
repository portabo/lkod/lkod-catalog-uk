"use client";
import { createContext, ReactNode, useContext } from "react";

import { IPublisherData } from "@/(schema)";

const OrganizationsContext = createContext<Promise<IPublisherData[]> | null>(null);

export const useOrganizationsPromise = () => {
    const organizations = useContext(OrganizationsContext);
    if (!organizations) {
        throw new Error("OrganizationsContext is not initialized");
    }
    return organizations;
};

const OrganizationsProvider = ({
    children,
    organizationsPromise,
}: {
    children: ReactNode;
    organizationsPromise: Promise<IPublisherData[]>;
}) => {
    return <OrganizationsContext.Provider value={organizationsPromise}>{children}</OrganizationsContext.Provider>;
};

export default OrganizationsProvider;
