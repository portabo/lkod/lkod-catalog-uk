"use client";
import { createContext, ReactNode, useContext } from "react";

import { IListFormat, IListKeyword, IListPublisher, IListTheme } from "@/(schema)";

type DataContextType = {
    publishersPromise: Promise<IListPublisher[]>;
    themesPromise: Promise<IListTheme[]>;
    keywordsPromise: Promise<IListKeyword[]>;
    formatsPromise: Promise<IListFormat[]>;
};

const DataContext = createContext<DataContextType | null>(null);

export const usePublishersPromise = () => {
    const publishers = useContext(DataContext)?.publishersPromise;
    if (!publishers) {
        throw new Error("DataContext (publishersPromise) is not initialized");
    }
    return publishers;
};

export const useThemesPromise = () => {
    const topics = useContext(DataContext)?.themesPromise;
    if (!topics) {
        throw new Error("DataContext (themesPromise) is not initialized");
    }
    return topics;
};

export const useKeywordsPromise = () => {
    const keywords = useContext(DataContext)?.keywordsPromise;
    if (!keywords) {
        throw new Error("DataContext (keywordsPromise) is not initialized");
    }
    return keywords;
};

export const useFormatsPromise = () => {
    const formats = useContext(DataContext)?.formatsPromise;
    if (!formats) {
        throw new Error("DataContext (formatsPromise) is not initialized");
    }
    return formats;
};

const DataProvider = ({
    children,
    publishersPromise,
    themesPromise,
    keywordsPromise,
    formatsPromise,
}: {
    children: ReactNode;
    publishersPromise: Promise<IListPublisher[]>;
    themesPromise: Promise<IListTheme[]>;
    keywordsPromise: Promise<IListKeyword[]>;
    formatsPromise: Promise<IListFormat[]>;
}) => {
    return (
        <DataContext.Provider value={{ publishersPromise, themesPromise, keywordsPromise, formatsPromise }}>
            {children}
        </DataContext.Provider>
    );
};

export default DataProvider;
