import parse from "html-react-parser";
import type { NextPage } from "next";
import { Metadata } from "next";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Meta from "@/metatags.json";
import text from "@/textContent/cs.json";

import styles from "./Cookies.module.scss";

const PAGE_LABEL = Meta.privacyPolicyPage.title;

export const metadata: Metadata = {
    title: `${text.configurable.siteTitle} | ${Meta.privacyPolicyPage.title}`,
};

const Project: NextPage = () => {
    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <Heading tag={`h1`}>{text.privacyPolicyPage.pageTitle}</Heading>
            <section className={styles["text"]}>
                <p>{parse(text.privacyPolicyPage.privacyPolicyDescription)}</p>
                <p>{parse(text.privacyPolicyPage.MoreInformation)}</p>
            </section>
        </Container>
    );
};

export default Project;
