import pino, { Logger } from "pino";
import { config } from "src/config";

export const getLogger = (name: string): Logger => {
    return pino({ name, level: config.LOG_LEVEL || "info" });
};
