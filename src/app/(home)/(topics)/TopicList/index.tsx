"use client";
import { useRouter } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { DetailedHTMLProps, forwardRef, HTMLAttributes, useState } from "react";

import TopicCard from "@/(home)/(topics)/TopicCard";
import { IThemeData } from "@/(schema)/common";
import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";

import styles from "./TopicList.module.scss";

const TOPICS_PER_PAGE = 8;

interface TopicList extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    label: string;
    data: IThemeData[];
}

const TopicList = forwardRef<HTMLDivElement, TopicList>(({ label, data }, ref) => {
    const router = useRouter();
    const plausible = usePlausible();

    const [perPage, setPerPage] = useState(TOPICS_PER_PAGE);

    const showHandler = () => {
        if (perPage === TOPICS_PER_PAGE) {
            setPerPage(data.length);
        } else setPerPage(TOPICS_PER_PAGE);
    };

    const pushTopicHandler = (topic: IThemeData) => {
        router.push(`/datasets?themeIris=${topic.iri}`);
        plausible("Topic+Click", {
            props: {
                topic: topic.title,
            },
        });
    };

    return (
        <div className={styles["topic-list"]} ref={ref} id="topicList">
            <Heading tag={`h2`} className={styles.inner}>
                {label}
            </Heading>
            <div className={styles.container}>
                {data.slice(0, perPage).map((topic) => {
                    return (
                        <TopicCard
                            key={topic.title}
                            label={topic.title}
                            topic={topic.image ? topic.image : ""}
                            count={topic.count}
                            onClick={() => pushTopicHandler(topic)}
                            disabled={topic.count === 0}
                        />
                    );
                })}
            </div>
            {perPage === TOPICS_PER_PAGE && (
                <ColorLink
                    className={styles["color-link"]}
                    linkText={text.colorLink.linkTextall}
                    onClick={showHandler}
                    center
                    direction={`down`}
                />
            )}
            {perPage !== TOPICS_PER_PAGE && (
                <ColorLink
                    className={styles["color-link"]}
                    style={{ marginTop: "2rem" }}
                    linkText={text.colorLink.linkText}
                    onClick={showHandler}
                    center
                    direction={`up`}
                />
            )}
        </div>
    );
});

export default TopicList;
