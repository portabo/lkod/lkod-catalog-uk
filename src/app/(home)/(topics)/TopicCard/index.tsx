import Image from "next/image";
import React, { FC } from "react";

import { Card } from "@/components/Card";

import styles from "./TopicCard.module.scss";

type TopicProps = { label: string; count: number; topic: string; onClick: () => void; disabled: boolean };

const TopicCard: FC<TopicProps> = ({ label, count, topic, onClick, disabled }) => {
    return (
        <Card tag={`button`} className={styles["topic-card"]} onClick={onClick} disabled={disabled} label={label}>
            <span className={styles["topic-card-icon"]}>
                <Image src={topic} alt={label} width={24} height={24} />
            </span>
            <span className={styles["topic-card-label"]}>{label}</span>
            <span className={styles["topic-card-link"]}>{`${count}
                ${count > 4 || count === 0 ? "datových sad" : count > 1 ? "datové sady" : "datová sada"}`}</span>
        </Card>
    );
};

export default TopicCard;
