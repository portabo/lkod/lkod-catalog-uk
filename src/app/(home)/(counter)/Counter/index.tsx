import Link from "next/link";
import React, { FC } from "react";

import { Digit } from "../Digit";
import styles from "./Counter.module.scss";

type CounterProps = {
    datasetsCount: number;
    themesCount: string;
    orgsCount: number;
};

const Underline = () => {
    return <span className={styles.underline} />;
};

export const Counter: FC<CounterProps> = ({ datasetsCount, themesCount, orgsCount }) => {
    return (
        <div className={styles["counter-container"]} data-testid="counter">
            <div className={styles.counter}>
                <Link href={`/organizations`}>
                    <Digit amount={orgsCount} caption="organizací" />
                </Link>
                <Underline />
                <Link href={`/datasets`}>
                    <Digit amount={datasetsCount} caption="datových sad" />
                </Link>
                <Underline />
                <Link href={`#topicList`}>
                    <Digit amount={themesCount} caption="témat" />
                </Link>
            </div>
        </div>
    );
};
