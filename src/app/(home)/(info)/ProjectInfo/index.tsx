import Image from "next/image";
import React from "react";
import { config } from "src/config";

import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import mainBackgroundImage from "@/images/mainBackgroundImage.png";
import text from "@/textContent/cs.json";

import styles from "./ProjectInfo.module.scss";

export const ProjectInfo = () => {
    return (
        <section className={styles["project-info"]}>
            <Image src={mainBackgroundImage} alt={`lkod background`} fill quality={70} />
            <div className={styles["text-field"]}>
                <Heading tag={`h2`}>{text.navBar.aboutCatalog}</Heading>
                <p>{text.aboutCatalogText}</p>
                {config.SHOW_PROJECT_PAGE !== "false" && (
                    <ColorLink linkText={text.colorLink.showMore} linkUrl="/about-open-data" start direction={`right`} />
                )}
            </div>
        </section>
    );
};
