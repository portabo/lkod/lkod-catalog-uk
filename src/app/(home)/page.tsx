import React, { Suspense } from "react";

import ColorLink from "@/components/ColorLink";
import { BackgroundPicture } from "@/components/configurable/BackgroundPicture";
import { SiteTitle } from "@/components/configurable/SiteTitle/index";
import { SearchForm } from "@/components/search/SearchForm";
import { getLogger } from "@/logging/logger";
import { CatalogService } from "@/services/catalog-service";
import text from "@/textContent/cs.json";

import Loading from "../loading";
import { Counter } from "./(counter)/Counter";
import { ProjectInfo } from "./(info)/ProjectInfo";
import HorizontalList from "./(organizations)/HorizontalList";
import TopicList from "./(topics)/TopicList";
import styles from "./Home.module.scss";

const logger = getLogger("Home");

const Home = async () => {
    const promises = await Promise.all([
        CatalogService.getAllPublishersData(),
        CatalogService.getAllThemesData(),
        CatalogService.findDatasets({
            offset: 0,
        }),
    ]);

    const organizations = promises[0];
    const topics = promises[1];
    const { count } = promises[2];

    const pregnantOrgs = organizations.filter((org) => org.count > 0);

    logger.debug({
        provider: "server",
        page: "Home",
        message: `${pregnantOrgs.length} organizations, ${topics.length} topics, ${count} datasets`,
    });

    return (
        <main id="main" className={styles.main}>
            <BackgroundPicture isHomepage />
            <div className={styles.container}>
                <SiteTitle />
                <Suspense fallback={<Loading />}>
                    {(pregnantOrgs.length !== 0 || topics.length !== 0) && (
                        <Counter orgsCount={pregnantOrgs.length} datasetsCount={count} themesCount={topics.length.toString()} />
                    )}
                </Suspense>
                <SearchForm />
                <ColorLink
                    className={styles["color-link"]}
                    linkText={text.colorLink.allDatasets}
                    linkUrl="/datasets"
                    direction={`right`}
                />
                <Suspense fallback={<Loading />}>
                    {topics.length !== 0 && <TopicList label={text.mainPage.topics} data={topics} />}
                </Suspense>
            </div>
            <Suspense fallback={<Loading />}>
                {pregnantOrgs.length !== 0 && <HorizontalList label={text.mainPage.organizations} data={pregnantOrgs} />}
            </Suspense>
            <ProjectInfo />
        </main>
    );
};

export default Home;
