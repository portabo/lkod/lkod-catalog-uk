"use client";
import { useRouter } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { FC, useCallback, useEffect, useRef, useState } from "react";

import { IPublisherData } from "@/(schema)/common";
import { ButtonScroll } from "@/components/ButtonScroll";
import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import text from "@/textContent/cs.json";
import { joinUrlPaths } from "@/utils/joinUrlPaths";

import HorizontalCard from "../HorizontalCard";
import styles from "./HorizontalList.module.scss";

type Props = { label: string; data: IPublisherData[] };

const HorizontalList: FC<Props> = ({ label, data }) => {
    const router = useRouter();
    const plausible = usePlausible();

    const [showLeftScroll, setShowLeftScroll] = useState<boolean>(false);
    const [showRightScroll, setShowRightScroll] = useState<boolean>(true);
    const size: ISize = useWindowSize();
    const elRef = useRef<HTMLButtonElement>(null);
    const scrollContainerRef = useRef<HTMLDivElement>(null);
    const scrollElement = (width: number) => {
        if (scrollContainerRef.current != null) {
            scrollContainerRef.current.scrollLeft += width;
        }
    };

    const handleScroll = useCallback(() => {
        const div = scrollContainerRef.current;
        if (div != null) {
            div.scrollLeft > 0 ? setShowLeftScroll(true) : setShowLeftScroll(false);
            div.scrollWidth - div.scrollLeft - div.clientWidth < 20 ? setShowRightScroll(false) : setShowRightScroll(true);
        }
    }, []);

    const pushOrgHandler = (publisher: IPublisherData) => {
        if (publisher.count === 0) return;
        if (publisher.slug) {
            router.push(`/${joinUrlPaths("organizations", publisher.slug)}`);
        }
        plausible("Organization+Click", {
            props: {
                publisher: publisher.name,
            },
        });
    };

    const SCROLL_SHIFT = size.width && size.width > 2500 ? 500 : 360;

    useEffect(() => {
        const div = scrollContainerRef.current;
        if (div) {
            div.addEventListener("scroll", handleScroll);
        }
    }, [handleScroll]);

    return (
        <div className={styles["horizontal-list"]}>
            <div className={styles["horizontal-list__heading-link"]}>
                <Heading tag={`h2`} className={styles.inner}>
                    {label}
                </Heading>
                <ColorLink linkText={text.colorLink.showall} linkUrl="/organizations" direction={`right`} end />
            </div>
            <div className={`${styles["scroll-nav"]} ${styles["scroll-nav__left"]} ${showLeftScroll ? "" : styles.hidden}`}>
                <ButtonScroll onClick={() => scrollElement(-SCROLL_SHIFT)} direction={`left`} ariaLabel={`scroll left`} />
            </div>
            <div className={`${styles["scroll-nav"]} ${styles["scroll-nav__right"]} ${showRightScroll ? "" : styles.hidden}`}>
                <ButtonScroll onClick={() => scrollElement(SCROLL_SHIFT)} direction={`right`} ariaLabel={`scroll right`} />
            </div>
            <div className={styles["scroll-container"]} ref={scrollContainerRef}>
                {data.map((item, i) => {
                    return (
                        <HorizontalCard
                            ref={elRef}
                            key={i}
                            label={item.name}
                            logoUrl={item.logo ?? ""}
                            onClick={() => pushOrgHandler(item)}
                            count={item.count}
                            disabled={item.count === 0}
                        />
                    );
                })}
            </div>
        </div>
    );
};

export default HorizontalList;
