import Image from "next/image";
import React, { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import { Card } from "@/components/Card";
import Project from "@/project.custom.json";

import styles from "./HorizontalCard.module.scss";

const errorNoImage = Project.helpers.errorNoImage;
interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    label: string;
    logoUrl?: string;
    count: number;
    onClick: () => void;
    disabled: boolean;
}

const HorizontalCard = forwardRef<HTMLButtonElement, Props>(({ label, logoUrl, count, onClick, disabled }, ref) => {
    return (
        <Card
            tag="button"
            horizontal="true"
            ref={ref}
            onClick={onClick}
            disabled={disabled}
            label={label}
            className={`${styles["horizontal-card-button"]}`}
        >
            <span className={`${styles["horizontal-card"]}`}>
                <span
                    className={`${styles["horizontal-card__image-frame"]} ${
                        logoUrl ? "" : styles["horizontal-card__image-frame_dull"]
                    }`}
                >
                    <Image
                        src={logoUrl ? logoUrl : errorNoImage}
                        alt={label}
                        fill
                        sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw"
                    />
                </span>
                <span>
                    <span className={styles["horizontal-card__label"]}>{label}</span>
                </span>
                <span>
                    <span className={styles["horizontal-card__link"]}>{`${count} ${
                        count > 4 || count === 0 ? "datových sad" : count > 1 ? "datové sady" : "datová sada"
                    }`}</span>
                </span>
            </span>
        </Card>
    );
});

HorizontalCard.displayName = "HorizontalCard";

export default HorizontalCard;
