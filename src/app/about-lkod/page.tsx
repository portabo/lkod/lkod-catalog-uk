import parse from "html-react-parser";
import { Metadata } from "next";
import Image from "next/image";
import React from "react";

import { Breadcrumb } from "@/components/Breadcrumb";
import { Card } from "@/components/Card";
import { BackgroundPicture } from "@/components/configurable/BackgroundPicture";
import { Heading } from "@/components/Heading";
import AccessibilityIcon from "@/components/icons/AccessibilityIcon";
import ListIcon from "@/components/icons/ListIcon";
import OfnIcon from "@/components/icons/OfnIcon";
import OpenSourceIcon from "@/components/icons/OpenSourceIcon";
import ImageChange from "@/components/ImageChange";
import { LangSwitch } from "@/components/LangSwitch";
import { Paragraph } from "@/components/Paragraph";
import LKOD_Administrace from "@/images/lkod-administration.webp";
import hierarchyImg from "@/images/lkod-hierarchy.svg";
import hierarchyImgEn from "@/images/lkod-hierarchyEn.svg";
import Meta from "@/metatags.json";
import textCz from "@/textContent/cs.json";
import textEn from "@/textContent/en.json";

import styles from "./AboutLkod.module.scss";

const PAGE_LABEL = Meta.aboutPage.title;

type params = {
    searchParams: {
        lang: string;
    };
};

export const metadata: Metadata = {
    title: `${textCz.configurable.siteTitle} | ${Meta.aboutPage.title}`,
    openGraph: {
        title: `${textCz.configurable.siteTitle} | ${Meta.aboutPage.title}`,
        images: [Meta.aboutPage.ogImage],
    },
    twitter: {
        card: "summary_large_image",
        images: [Meta.aboutPage.twitterImage],
    },
};

const AboutLkod = ({ searchParams }: params) => {
    const { lang } = searchParams;
    const isCzech = lang !== "en";

    const text = lang === "en" ? textEn : textCz;

    return (
        <main id="main" className={styles.main}>
            <div className={styles.container}>
                <Breadcrumb labels={[{ label: isCzech ? PAGE_LABEL : text.aboutLkodPage.aboutLkodPageTitle, link: "" }]} />
                <BackgroundPicture />
                <div className={styles["aboutlkod-container"]}>
                    <div className={styles["head-row"]}>
                        <Heading tag={`h1`}>{isCzech ? PAGE_LABEL : text.aboutLkodPage.aboutLkodPageTitle}</Heading>
                        <LangSwitch />
                    </div>
                    <p>{text.aboutLkodPage.aboutLkodPageDescription}</p>
                    <Heading className={styles["heading"]} tag={`h2`}>
                        {text.aboutLkodPage.lkod}
                    </Heading>
                    <section className={styles["container-purpose"]}>
                        <div className={styles["text-field"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.whatIsLC}
                            </Heading>
                            <Card tag="div" className={styles["text-field-card"]}>
                                <ListIcon values="1" />
                                <h4>{text.aboutLkodPage.whatIsLCparagraph1}</h4>
                            </Card>
                            <Card tag="div" className={styles["text-field-card"]}>
                                <ListIcon values="2" />
                                <h4>{text.aboutLkodPage.whatIsLCparagraph2}</h4>
                            </Card>
                        </div>
                        <div className={styles["image-field"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.hierarchyLC}
                            </Heading>
                            <Image src={isCzech ? hierarchyImg : hierarchyImgEn} alt="catalog hierarchy image" />
                        </div>
                    </section>

                    <Heading className={styles["heading"]} tag={`h2`}>
                        {text.aboutLkodPage.wantToUseLKODTitle}
                    </Heading>

                    <section className={styles["container-uselkod"]}>
                        <div className={styles["container-uselkod__card"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.forOrganizationsTitle}
                            </Heading>
                            <p className={isCzech ? "" : styles["red-link-inside"]}>
                                {parse(text.aboutLkodPage.forOrganizations)}
                                {isCzech && (
                                    <a href={`mailto:${text.aboutLkodPage.emailForOrganizations}`} className={styles["red-link"]}>
                                        {text.aboutLkodPage.emailForOrganizations}
                                    </a>
                                )}
                            </p>
                        </div>
                        <div className={styles["container-uselkod__card"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.lCTitle}
                            </Heading>
                            <p>{parse(text.aboutLkodPage.lCDescription)}</p>
                        </div>
                        <div className={styles["container-uselkod__card"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.migrationTitle}
                            </Heading>
                            <p>{parse(text.aboutLkodPage.migrationDescription)}</p>
                        </div>
                    </section>

                    <Heading className={styles["heading"]} tag={`h2`}>
                        {text.aboutLkodPage.technicallyLKODTitle}
                    </Heading>

                    <section className={styles["container-lkodtech"]}>
                        <div className={styles["lkodtech-text"]}>
                            <p>{text.aboutLkodPage.technicallyLKODDescription}</p>
                        </div>
                        <div className={styles["lkodtech-image"]}>
                            <ImageChange isCzech={isCzech} />
                        </div>
                    </section>
                </div>
            </div>
            <div className={styles["administration-block-wrapper"]}>
                <div className={styles["administration-block"]}>
                    <Heading className={styles["heading"]} tag={`h2`}>
                        {text.aboutLkodPage.lKODAdministrationTitle}
                    </Heading>
                    <div className={styles["administration-list"]}>
                        <p>
                            <b>{text.aboutLkodPage.lKODAdministrationDescription}</b>
                        </p>
                        <ul>
                            <li>
                                <ListIcon color="secondary" width="24px" height="24px" values="1" />
                                <p>{parse(text.aboutLkodPage.lKODAdministrationparagraph1)}</p>
                            </li>

                            <li>
                                <ListIcon color="secondary" width="24px" height="24px" values="2" />
                                {text.aboutLkodPage.lKODAdministrationparagraph2}
                            </li>
                            <li>
                                <ListIcon color="secondary" width="24px" height="24px" values="3" />
                                {text.aboutLkodPage.lKODAdministrationparagraph3}
                            </li>
                            <li>
                                <ListIcon color="secondary" width="24px" height="24px" values="4" />
                                {text.aboutLkodPage.lKODAdministrationparagraph4}
                            </li>
                            <li>
                                <ListIcon color="secondary" width="24px" height="24px" values="5" />
                                {text.aboutLkodPage.lKODAdministrationparagraph5}
                            </li>
                        </ul>
                    </div>
                    <div className={styles["image-box"]}>
                        <Image src={LKOD_Administrace} alt={"LKOD_administrace"} />
                    </div>
                </div>
            </div>
            <div className={styles.container}>
                <section className={styles["container-links"]}>
                    <Card tag="div" className={styles["link-card"]}>
                        <AccessibilityIcon width={27} height={28} />
                        <div className={styles["link-card-text"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.accessibilityTitle}
                            </Heading>
                            <a href="https://opendata.praha.eu/accessibility" target="_blank" rel="noreferrer">
                                <Paragraph size="md">{text.aboutLkodPage.accessibilityDescription}</Paragraph>
                            </a>
                        </div>
                    </Card>
                    <Card tag="div" className={styles["link-card"]}>
                        <OpenSourceIcon width={30} height={28} />
                        <div className={styles["link-card-text"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.openSourceTitle}
                            </Heading>
                            <a
                                href="https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/tree/master/demo"
                                target="_blank"
                                rel="noreferrer"
                            >
                                <Paragraph size="md">{text.aboutLkodPage.openSourceDescription}</Paragraph>
                            </a>
                        </div>
                    </Card>
                    <Card tag="div" className={styles["link-card"]}>
                        <OfnIcon width={21} height={28} />
                        <div className={styles["link-card-text"]}>
                            <Heading tag="h3" type="h5">
                                {text.aboutLkodPage.ofnTitle}
                            </Heading>
                            <a
                                href="https://ofn.gov.cz/rozhran%C3%AD-katalog%C5%AF-otev%C5%99en%C3%BDch-dat/2021-01-11/"
                                target="_blank"
                                rel="noreferrer"
                            >
                                <Paragraph size="md">{text.aboutLkodPage.ofnDescription}</Paragraph>
                            </a>
                        </div>
                    </Card>
                </section>
            </div>
        </main>
    );
};

export default AboutLkod;
