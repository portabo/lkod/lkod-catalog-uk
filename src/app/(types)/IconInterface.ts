export interface IiCon {
    color: "primary" | "secondary" | "tertiary" | "black";
    direction?: "up" | "down" | "left" | "right";
}
