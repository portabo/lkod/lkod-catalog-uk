import noOkIcon from "@/images/no-ok-icon.svg";
import okIcon from "@/images/ok-icon.svg";

export const getCopyright = (value: string | undefined | null) => {
    const decodedValue = value ? decodeURIComponent(value) : null;

    if (decodedValue === "https://data.gov.cz/podmínky-užití/neobsahuje-autorská-díla/") {
        return {
            text: "Neobsahuje autorská díla.",
            icon: okIcon,
            link: value,
        };
    }
    if (decodedValue === "https://data.gov.cz/podmínky-užití/obsahuje-více-autorských-děl/") {
        return {
            text: "Obsahuje více autorských děl.",
            icon: noOkIcon,
            link: value,
        };
    }
    if (value === null) {
        return {
            text: "Informace nenalezeny",
            icon: noOkIcon,
            link: value,
        };
    }
    return {
        text: "Autorské dílo. Otevřít licenci.",
        icon: noOkIcon,
        link: value,
    };
};

export const getCopyrightedDB = (value: string | undefined | null) => {
    const decodedValue = value ? decodeURIComponent(value) : null;

    if (decodedValue === "https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/") {
        return { text: "Není autorskoprávně chráněnou databází.", icon: okIcon, link: value };
    }
    if (value === null) {
        return {
            text: "Informace nenalezeny",
            icon: noOkIcon,
            link: value,
        };
    }
    return { text: "Autorskoprávně chráněná databáze. Otevřít licenci.", icon: noOkIcon, link: value };
};

export const getSpecialDB = (value: string | undefined | null) => {
    const decodedValue = value ? decodeURIComponent(value) : null;

    if (decodedValue === "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/") {
        return { text: "Není chráněn zvláštním právem pořizovatele databáze.", icon: okIcon, link: value };
    }
    if (value === null) {
        return {
            text: "Informace nenalezeny",
            icon: noOkIcon,
            link: value,
        };
    }
    return { text: "Zvláštní právo pořizovatele databáze. Otevřít licenci.", icon: noOkIcon, link: value };
};

export const getPII = (value: string | undefined | null) => {
    const decodedValue = value ? decodeURIComponent(value) : null;

    if (decodedValue === "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/") {
        return {
            text: "Neobsahuje osobní údaje.",
            icon: okIcon,
            link: value,
        };
    }
    if (decodedValue === "https://data.gov.cz/podmínky-užití/obsahuje-osobní-údaje/") {
        return {
            text: "Obsahuje osobní údaje.",
            icon: noOkIcon,
            link: value,
        };
    }
    if (value === null) {
        return {
            text: "Informace nenalezeny",
            icon: noOkIcon,
            link: value,
        };
    }
    return {
        text: "Může obsahovat osobní údaje. Více informací.",
        icon: noOkIcon,
        link: value,
    };
};
