import { config } from "src/config";

import { IListTheme, IThemeData } from "@/(schema)";
import { lastUrlPart } from "@/utils/lastUrlPart";

export const transformThemes = (themes: IListTheme[]): IThemeData[] => {
    return themes.map((theme) => ({
        iri: theme.iri,
        title: theme.label,
        count: Number(theme.count),
        image: `${config.ICONS_URL}/${lastUrlPart(theme.iri)}.svg`,
    }));
};
