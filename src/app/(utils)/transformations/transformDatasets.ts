import { config } from "src/config";

import { DatasetJsonItem, DatasetsItem } from "@/(schema)";

import { lastUrlPart } from "../lastUrlPart";

export const transformDatasets = (datasets: DatasetJsonItem[]): DatasetsItem[] => {
    return datasets.map((dataset) => ({
        iri: dataset.iri,
        title: dataset.title,
        description: dataset.description,
        publisher: dataset.publisher,
        themeNames: dataset.themes,
        formats: dataset.formats,
        themeImage: dataset.theme_iris.length > 0 ? `${config.ICONS_URL}/${lastUrlPart(dataset.theme_iris[0])}.svg` : "",
    }));
};
