import { config } from "src/config";

import { Dataset, DatasetJson, DistributionJson } from "@/(schema)";

import { lastUrlPart } from "../lastUrlPart";

export const transformDataset = (dataset: DatasetJson): Dataset => {
    return {
        iri: dataset.iri,
        title: dataset.title,
        description: dataset.description,
        publisher: dataset.publisher,
        documentation: dataset.documentation,
        specification: dataset.specification,
        accrualPeriodicity: dataset.accrual_periodicity ? dataset.accrual_periodicity.label : null,
        temporalFrom: dataset.temporal_from,
        temporalTo: dataset.temporal_to,
        temporalResolution: dataset.temporal_resolution,
        spatialResolutionInMeters: dataset.spatial_resolution_in_meters,
        contactPoint: dataset.contact_point,
        keywords: dataset.keywords,
        themes: dataset.themes.map((theme) => ({
            iri: theme.iri,
            title: theme.title,
            image: `${config.ICONS_URL}/${lastUrlPart(theme.iri)}.svg`,
        })),
        eurovocThemes: dataset.eurovoc_themes,
        distributions: {
            files: dataset.distributions
                .filter((distribution: DistributionJson) => distribution.format.iri)
                .map((distribution: DistributionJson) => ({
                    distributionIri: distribution.iri,
                    title: distribution.title,
                })),
            services: dataset.distributions
                .filter((distribution: DistributionJson) => distribution.access_service?.iri)
                .map((distribution: DistributionJson) => ({
                    distributionIri: distribution.iri,
                    title: distribution.title,
                    accessServiceIri: distribution.access_url,
                })),
        },
        spatial: dataset.spatial,
    };
};
