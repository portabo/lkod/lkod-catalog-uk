import { DatasetJson, DistributionJson, DistributionService } from "@/(schema)";

export const transformDatasetToDistributionServices = (dataset: DatasetJson): Array<DistributionService> => {
    return dataset.distributions.map((distribution: DistributionJson) => ({
        iri: distribution.access_service ? distribution.access_service.iri : "",
        title: distribution.access_service ? distribution.access_service.title : "",
        endpointURL: distribution.access_service ? distribution.access_service?.endpoint_url : "",
        endpointDescription: distribution.access_service ? distribution.access_service?.endpoint_description : "",
        conformsTo: distribution.conforms_to,
    }));
};
