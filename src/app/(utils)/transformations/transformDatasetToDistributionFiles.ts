import { DatasetJson, DistributionJson } from "@/(schema)";
import { DistributionFile } from "@/datasets/(distributions)/DistributionFiles";

export const transformDatasetToDistributionFiles = (dataset: DatasetJson): Array<DistributionFile> => {
    return dataset.distributions
        .filter((distribution: DistributionJson) => !distribution.access_service)
        .map((distribution: DistributionJson) => ({
            iri: distribution.iri,
            title: distribution.title,
            format: {
                iri: distribution.format.iri,
                title: distribution.format.title,
            },
            mediaType: distribution.media_type,
            downloadUrl: distribution.download_url,
            accessUrl: distribution.access_url,
            compressFormat: distribution.compress_format,
            packageFormat: distribution.package_format,
            conformsTo: distribution.conforms_to,
            licence: {
                "autorské-dílo": distribution.licence.copyright_licence ?? null,
                "databáze-jako-autorské-dílo": distribution.licence.database_copyright_licence ?? null,
                "databáze-chráněná-zvláštními-právy": distribution.licence.database_protected_by_law ?? null,
                "osobní-údaje": distribution.licence.personal_data ?? null,
            },
        }));
};
