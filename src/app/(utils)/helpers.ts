export const stringOrArr = (data: string | string[]) => {
    return typeof data === "string" ? [data] : [...data];
};
