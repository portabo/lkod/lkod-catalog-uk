import path from "path";

export const joinUrlPaths = (...paths: string[]): string => {
    const [baseUrl, ...restPaths] = paths;
    const isAbsolute = baseUrl.startsWith("https://") || baseUrl.startsWith("http://");
    if (!isAbsolute) {
        return path.posix.join(...paths);
    }
    const base = new URL(baseUrl);
    return new URL(path.posix.join(base.pathname, ...restPaths), base).toString();
};
