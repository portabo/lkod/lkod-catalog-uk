import { MetadataRoute } from "next";

import { CatalogService } from "./(services)/catalog-service";

export const dynamic = "force-dynamic";

export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
    const BaseUrl = "https://opendata.praha.eu";
    const { datasets } = await CatalogService.findDatasets({
        limit: 1000,
        offset: 0,
    });

    const datasetsUrl = datasets.map((dataset) => {
        const validChangeFrequency: MetadataRoute.Sitemap[0]["changeFrequency"] = "daily";
        const encodedIri = encodeURIComponent(dataset.iri);

        return {
            url: `${BaseUrl}/datasets/${encodedIri}`,
            lastModified: new Date(),
            changeFrequency: validChangeFrequency,
            priority: 0.8,
        };
    });

    const organizations = await CatalogService.getAllPublishersData();

    const organizationsUrl = organizations
        .filter((org) => org.count > 0)
        .map((organization) => {
            const validChangeFrequency: MetadataRoute.Sitemap[0]["changeFrequency"] = "daily";

            return {
                url: `${BaseUrl}/organizations/${organization.slug}`,
                lastModified: new Date(),
                changeFrequency: validChangeFrequency,
                priority: 0.8,
            };
        });

    return [
        {
            url: BaseUrl,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 1,
        },
        {
            url: `${BaseUrl}/datasets`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.8,
        },
        {
            url: `${BaseUrl}/organizations`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.8,
        },
        {
            url: `${BaseUrl}/about-open-data`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.5,
        },
        {
            url: `${BaseUrl}/about-open-data/what-is-open-data`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.5,
        },
        {
            url: `${BaseUrl}/about-open-data/open-data-use-and-contribution`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.5,
        },
        {
            url: `${BaseUrl}/about-open-data/open-data-publish`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.5,
        },
        {
            url: `${BaseUrl}/about-lkod`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.5,
        },
        {
            url: `${BaseUrl}/accessibility`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.5,
        },
        {
            url: `${BaseUrl}/cookies`,
            lastModified: new Date(),
            changeFrequency: "daily",
            priority: 0.5,
        },
        ...datasetsUrl,
        ...organizationsUrl,
    ];
}
