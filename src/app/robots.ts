import { MetadataRoute } from "next";

export default function robots(): MetadataRoute.Robots {
    const BaseUrl = "https://opendata.praha.eu";

    return {
        rules: {
            userAgent: "*",
            allow: "/",
        },
        host: BaseUrl,
        sitemap: `${BaseUrl}/sitemap.xml`,
    };
}
