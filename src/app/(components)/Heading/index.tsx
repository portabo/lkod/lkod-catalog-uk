import { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import styles from "./Heading.module.scss";

type Tags = "h1" | "h2" | "h3" | "h4" | "h5" | "h6";

type Type = Tags;

interface HeadingProps extends DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement> {
    tag?: Tags;
    type?: Type;
}

export const Heading = forwardRef<HTMLHeadingElement, HeadingProps>(
    // eslint-disable-next-line react/prop-types
    ({ tag = "h1", type = tag, className, ...restProps }, ref) => {
        const Tag = tag;
        return <Tag className={`${styles.heading} ${styles[type]} ${className ?? ""}`} {...restProps} ref={ref} />;
    }
);
