import Image from "next/image";
import Link from "next/link";
import React, { forwardRef } from "react";

import logo from "@/logos/logo_uk.svg";
import text from "@/textContent/cs.json";

import styles from "./Logo.module.scss";

type LogoProps = {
    footer?: boolean;
};

export const Logo = forwardRef<HTMLDivElement, LogoProps>(({ footer }, ref) => {
    return (
        <Link href="/" className={`${styles["frame"]}`}>
            <div className={`${styles["logo-frame"]} ${footer ? styles["logo-frame_footer"] : ""}`} ref={ref}>
                <Image src={logo} alt={text.configurable.logoOwner} fill priority />
            </div>
            <div className={`${styles.text} ${footer ? styles["text-footer"] : undefined}`}>{text.configurable.logoText}</div>
        </Link>
    );
});
