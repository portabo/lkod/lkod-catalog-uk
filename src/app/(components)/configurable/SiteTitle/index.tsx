import React, { FC } from "react";

import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";

import styles from "./SiteTitle.module.scss";

export const SiteTitle: FC = () => {
    return (
        <Heading tag={`h1`} className={`${styles["site-title"]} ${styles["site-title__first-line"]}`}>
            {text.configurable.siteTitle}
        </Heading>
    );
};
