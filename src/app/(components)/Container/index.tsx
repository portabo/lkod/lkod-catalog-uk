import React, { CSSProperties, FC, ReactNode } from "react";

import styles from "./Container.module.scss";

const Container: FC<{ children: ReactNode; style?: CSSProperties }> = ({ children, style }) => {
    return (
        <main id="main" className={styles.main}>
            <div className={styles.container} style={style}>
                {children}
            </div>
        </main>
    );
};

export default Container;
