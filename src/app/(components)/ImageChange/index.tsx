"use client";
import Image from "next/image";

import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import LKOD_Technically from "@/images/lkod-technically.svg";
import LKOD_Technically_SmallScreen from "@/images/lkod-technically-media.svg";
import LKOD_Technically_SmallScreenEn from "@/images/lkod-technically-mediaEn.svg";
import LKOD_TechnicallyEn from "@/images/lkod-technicallyEn.svg";

type ImageChangeProps = {
    isCzech: boolean;
};

const ImageChange = ({ isCzech }: ImageChangeProps) => {
    const size: ISize = useWindowSize();

    const images = {
        smallScreen: {
            cz: LKOD_Technically_SmallScreen,
            en: LKOD_Technically_SmallScreenEn,
        },
        largeScreen: {
            cz: LKOD_Technically,
            en: LKOD_TechnicallyEn,
        },
    };

    const screenType = size.width && size.width < 576 ? "smallScreen" : "largeScreen";
    const language = isCzech ? "cz" : "en";

    return <Image src={images[screenType][language]} alt="LKOD_technically" />;
};

export default ImageChange;
