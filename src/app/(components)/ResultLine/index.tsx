import React, { FC } from "react";

import styles from "./ResultLine.module.scss";

type Props = {
    result: number;
    organization?: boolean;
    orgPage?: boolean;
};

export const ResultLine: FC<Props> = ({ result, organization, orgPage }) => {
    result = +result;
    return (
        <span className={styles["result-line"]}>
            {orgPage ? <span className={styles["for-orgpage"]}>{`Datové sady dané organizace`}</span> : false}
            {`${result === 0 || result > 4 ? "Nalezeno" : result > 1 ? "Nalezeny" : "Nalezena"}  ${result} ${
                organization
                    ? "organizací"
                    : result > 4 || result === 0
                      ? "datových sad"
                      : result > 1
                        ? "datové sady"
                        : "datová sada"
            }`}{" "}
        </span>
    );
};
