import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import { HomeIcon } from "@/components/icons/HomeIcon";

import styles from "./Breadcrumb.module.scss";

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLUListElement>, HTMLUListElement> {
    labels: { label: string; link: string }[];
}

export const Breadcrumb: FC<Props> = ({ labels }) => {
    return (
        <ul className={styles.breadcrumb}>
            <li>
                <a href={`/`} aria-label="Domů">
                    <HomeIcon width={18} height={20} />
                </a>
            </li>
            {labels.map((label, i) => {
                if (labels.length - 1 === i) {
                    return <li key={i}>{label.label}</li>;
                } else {
                    return (
                        <li key={i}>
                            <a href={label.link} aria-label={label.label}>
                                {label.label}
                            </a>
                        </li>
                    );
                }
            })}
        </ul>
    );
};
