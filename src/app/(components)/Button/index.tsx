/* eslint-disable react/display-name */
import React, { ButtonHTMLAttributes, DetailedHTMLProps, forwardRef, ReactNode, Ref } from "react";

import styles from "./Button.module.scss";

type Colors = "secondary" | "primary" | "outline-primary" | "outline-gray" | "transparent";

interface ButtonProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    label?: string;
    color: Colors;
    iconStart?: ReactNode;
    iconEnd?: ReactNode;
    hideLabelMobile?: boolean;
    hideLabel?: boolean;
}

const Button = forwardRef<HTMLButtonElement, ButtonProps>((componentProps: ButtonProps, ref: Ref<HTMLButtonElement>) => {
    const {
        className,
        color,
        children,
        label,
        iconStart,
        iconEnd,
        onClick,
        type = "button",
        form,
        disabled,
        hideLabelMobile,
        hideLabel,
        id,
    } = componentProps;

    return (
        <button
            ref={ref}
            onClick={onClick}
            type={type}
            form={form}
            className={`${styles["button"]} ${styles[`button_color_${color}`]} ${className}`}
            disabled={disabled}
            aria-label={label}
            id={id}
        >
            {iconStart && <span className={`${styles["icon-frame"]}`}>{iconStart}</span>}
            {children}
            {!hideLabel && label && <span className={`${hideLabelMobile ? styles.label : ""}`}>{label}</span>}
            {iconEnd && <span className={`${styles["icon-frame"]}`}>{iconEnd}</span>}
        </button>
    );
});

export default Button;
