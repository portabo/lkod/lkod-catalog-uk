/* eslint-disable max-len */
import * as React from "react";
import { SVGProps } from "react";
interface EducationAndLinksIconProps extends SVGProps<SVGSVGElement> {
    disabled?: boolean;
    color?: string;
    width?: string;
    height?: string;
}

const FolderIcon: React.FC<EducationAndLinksIconProps> = ({
    disabled = false,
    color = "#ED2232",
    width = "28",
    height = "38",
    ...props
}) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        fill={disabled ? "#F0A2A8" : color}
        width={width}
        height={height}
        viewBox="0 0 64 80"
        {...props}
    >
        <path
            d="M9 10c-2.75 0-5 2.25-5 5v7.127A6.976 6.976 0 0 1 9 20h46c1.952 0 3.725.82 5 2.127V21c0-2.75-2.25-5-5-5H33a3.827 3.827 0 0 1-2.707-1.121l-3.172-3.172A5.83 5.83 0 0 0 23 10H9zm0 12c-2.75 0-5 2.25-5 5v22c0 2.75 2.25 5 5 5h46c2.75 0 5-2.25 5-5V27c0-2.75-2.25-5-5-5H9zm23.039 6c.782.01 1.53.326 2.082.879l7.006 7.004a3.016 3.016 0 0 1 0 4.244 3.017 3.017 0 0 1-4.244 0L35 38.244V45c0 1.645-1.355 3-3 3s-3-1.355-3-3v-6.756l-1.883 1.883a3.017 3.017 0 0 1-4.244 0 3.016 3.016 0 0 1 0-4.244l7.004-7.004A3.005 3.005 0 0 1 32.039 28z"
            style={{
                fontFeatureSettings: "normal",
                fontVariantAlternates: "normal",
                fontVariantCaps: "normal",
                fontVariantEastAsian: "normal",
                fontVariantLigatures: "normal",
                fontVariantNumeric: "normal",
                fontVariantPosition: "normal",
                fontVariationSettings: "normal",
                inlineSize: 0,
                isolation: "auto",
                mixBlendMode: "normal",
                shapeMargin: 0,
                textDecorationColor: "#000",
                textDecorationLine: "none",
                textDecorationStyle: "solid",
                textIndent: 0,
                textOrientation: "mixed",
                textTransform: "none",
                whiteSpace: "normal",
            }}
        />
    </svg>
);

export default FolderIcon;
