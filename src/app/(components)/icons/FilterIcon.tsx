import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const FilterIcon = ({ color, width = "100%", height = "100%", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        fill={fill || "none"}
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M1.117 1.648c.274-.546.82-.898 1.446-.898h16.875c.585 0 1.132.352 1.406.898a1.64 1.64 0 0 1-.235
            1.68L13.5 12.04V17c0 .508-.273.938-.703
            1.133-.43.195-.938.156-1.328-.117l-2.5-1.875c-.313-.235-.469-.586-.469-1.016v-3.086L1.352
            3.33a1.64 1.64 0 0 1-.235-1.68Z"
        />
    </svg>
);
