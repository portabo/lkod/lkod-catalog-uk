import * as React from "react";
import { SVGProps } from "react";
const ArrowIcon = (props: SVGProps<SVGSVGElement>) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={15} height={13} fill="none" {...props}>
        <path
            fill="#003366"
            // eslint-disable-next-line max-len
            d="m14.188 7.335-5 5a.97.97 0 0 1-.688.281.99.99 0 0 1-.719-.281.964.964 0 0 1 0-1.406l3.282-3.313H1.5c-.563 0-1-.437-1-1 0-.531.438-1 1-1h9.563L7.78 2.335a.964.964 0 0 1 0-1.406.964.964 0 0 1 1.407 0l5 5a.964.964 0 0 1 0 1.406Z"
        />
    </svg>
);
export default ArrowIcon;
