/* eslint-disable max-len */
import * as React from "react";
import { SVGProps } from "react";

export const FileCleanIcon = ({ width = "2.1rem", height = "3rem", ...props }: SVGProps<SVGSVGElement>) => (
    <svg width={width} height={height} viewBox="0 0 24 32" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
        <rect width={23.883} height={31.907} fill="white" style={{}} y={-0.005} />
        <g filter="url(#filter0_i_363_10923)" transform="matrix(1.19143, 0, 0, 1.19143, -7.06288, -3.658595)" style={{}}>
            <path
                d="M 17.667 10.25 L 17.667 3.167 L 7.25 3.167 C 6.557 3.167 6 3.724 6 4.417 L 6 28.583 C 6 29.276 6.557 29.833 7.25 29.833 L 24.75 29.833 C 25.443 29.833 26 29.276 26 28.583 L 26 11.5 L 18.917 11.5 C 18.229 11.5 17.667 10.938 17.667 10.25 Z M 26 9.516 L 26 9.833 L 19.333 9.833 L 19.333 3.167 L 19.651 3.167 C 19.984 3.167 20.302 3.297 20.537 3.531 L 25.635 8.635 C 25.87 8.87 26 9.187 26 9.516 Z"
                fill="url(#paint0_linear_363_10923)"
                fillOpacity={0.4}
            />
        </g>
        <defs>
            <filter
                id="filter0_i_363_10923"
                x={6}
                y={3.16663}
                width={20}
                height={30.6666}
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                <feColorMatrix
                    in="SourceAlpha"
                    type="matrix"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    result="hardAlpha"
                />
                <feOffset dy={4} />
                <feGaussianBlur stdDeviation={2} />
                <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0" />
                <feBlend mode="normal" in2="shape" result="effect1_innerShadow_363_10923" />
            </filter>
            <linearGradient
                id="paint0_linear_363_10923"
                x1={26.05}
                y1={3.16663}
                x2={0.43217}
                y2={22.428}
                gradientUnits="userSpaceOnUse"
            >
                <stop stopColor="#96AAB7" />
                <stop offset={1} stopColor="#728896" />
            </linearGradient>
        </defs>
    </svg>
);
