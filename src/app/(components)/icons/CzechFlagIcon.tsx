import React, { SVGProps } from "react";

export const CzechFlagIcon = ({ width = "19px", height = "14px", ...props }: SVGProps<SVGSVGElement>) => {
    return (
        <svg
            style={{ width: width, height: height }}
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 19 14"
            {...props}
        >
            <g id="Flag_of_the_Czech_Republic" clipPath="url(#clip0_6457_13674)">
                <path id="Vector" d="M19 0H0V14H19V0Z" fill="#D7141A" />
                <path id="Vector_2" d="M19 0H0V7H19V0Z" fill="white" />
                <path id="Vector_3" d="M9.5 7L0 0V14L9.5 7Z" fill="#11457E" />
            </g>
            <defs>
                <clipPath id="clip0_6457_13674">
                    <rect width="19" height="14" fill="white" />
                </clipPath>
            </defs>
        </svg>
    );
};
