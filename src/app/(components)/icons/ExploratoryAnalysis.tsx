/* eslint-disable max-len */

import { SVGProps } from "react";

const ExploratoryAnalysisIcon = ({ width = "34px", height = "32px" }: SVGProps<SVGSVGElement>) => (
    <svg width={width} height={height} viewBox="0 0 34 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g filter="url(#filter0_i_6790_14456)">
            <path
                d="M17 15V1.0625C17 0.5 17.4375 0 18 0C25.6875 0 32 6.3125 32 14C32 14.5625 31.5 15 30.9375 15H17ZM0 17C0 9.4375 5.625 3.1875 12.9375 2.1875C13.5 2.0625 14 2.5625 14 3.125V18L23.75 27.8125C24.1875 28.25 24.125 28.9375 23.6875 29.25C21.1875 31 18.1875 32 15 32C6.6875 32 0 25.3125 0 17ZM32.875 18C33.4375 18 33.9375 18.5 33.8125 19.0625C33.375 22.5625 31.6875 25.6875 29.1875 28C28.8125 28.3125 28.25 28.3125 27.875 27.9375L18 18H32.875Z"
                fill="url(#paint0_linear_6790_14456)"
                fill-opacity="0.4"
            />
        </g>
        <defs>
            <filter
                id="filter0_i_6790_14456"
                x="0"
                y="0"
                width="33.9375"
                height="36"
                filterUnits="userSpaceOnUse"
                color-interpolation-filters="sRGB"
            >
                <feFlood flood-opacity="0" result="BackgroundImageFix" />
                <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                <feColorMatrix
                    in="SourceAlpha"
                    type="matrix"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    result="hardAlpha"
                />
                <feOffset dy="4" />
                <feGaussianBlur stdDeviation="2" />
                <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0" />
                <feBlend mode="normal" in2="shape" result="effect1_innerShadow_6790_14456" />
            </filter>
            <linearGradient
                id="paint0_linear_6790_14456"
                x1="34.09"
                y1="2"
                x2="6.96829"
                y2="36.9579"
                gradientUnits="userSpaceOnUse"
            >
                <stop stop-color="#96AAB7" />
                <stop offset="1" stop-color="#728896" />
            </linearGradient>
        </defs>
    </svg>
);

export default ExploratoryAnalysisIcon;
