import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconStrokes.module.scss";

export const CloseIcon = ({ color, width = "1rem", height = "1rem", ...props }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <path strokeWidth={2} strokeLinecap="square" d="M1.449 1.531 12.418 12.5M12.418 1.531 1.449 12.5" />
    </svg>
);
