import React, { SVGProps } from "react";

export const FileIcon = ({ width = "2rem", height = "2.5rem", ...props }: SVGProps<SVGSVGElement>) => {
    return (
        <svg
            style={{ width: width, height: height }}
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 32 30"
            {...props}
        >
            <path fill="#fff" d="M0 -0.005H24.693V32.983999999999995H0z"></path>
            <g filter="url(#filter0_i_363_10923)" transform="matrix(1.23181 0 0 1.23181 -7.342 -3.856)">
                <path
                    fill="url(#paint0_linear_363_10923)"
                    fillOpacity="0.4"
                    d="M17.667 10.25V3.167H7.25c-.693 0-1.25.557-1.25 1.25v24.166c0 .693.557 
                    1.25 1.25 1.25h17.5c.693 0 1.25-.557 1.25-1.25V11.5h-7.083c-.688 0-1.25-.563-1.25-1.25zm2.974 
                    6.25h1.244c.401 0 .698.37.61.766l-1.98 8.75a.62.62 0 
                    01-.609.484h-1.979a.624.624 0 01-.604-.474c-1.344-5.39-1.083-4.23-1.333-5.755h-.027c-.057.745-.124.906-1.333 
                    5.755a.624.624 0 01-.604.474h-1.932a.624.624 0 01-.61-.49l-1.968-8.75a.623.623 0 01.609-.76h1.276c.297 
                    0 .557.208.615.505.812 4.063 1.046 5.703 1.093 6.365.084-.532.38-1.703 1.532-6.39a.62.62 
                    0 01.609-.475h1.516c.291 0 .541.198.609.48 1.25 5.229 1.5 
                    6.458 1.542 6.739-.01-.583-.136-.927 1.125-6.73a.604.604 
                    0 01.599-.494zM26 9.516v.317h-6.667V3.167h.318c.333 0 .651.13.886.364l5.098 5.104c.235.235.365.552.365.88z"
                ></path>
            </g>
            <defs>
                <filter
                    id="filter0_i_363_10923"
                    width="20"
                    height="30.667"
                    x="6"
                    y="3.167"
                    colorInterpolationFilters="sRGB"
                    filterUnits="userSpaceOnUse"
                >
                    <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
                    <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape"></feBlend>
                    <feColorMatrix
                        in="SourceAlpha"
                        result="hardAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    ></feColorMatrix>
                    <feOffset dy="4"></feOffset>
                    <feGaussianBlur stdDeviation="2"></feGaussianBlur>
                    <feComposite in2="hardAlpha" k2="-1" k3="1" operator="arithmetic"></feComposite>
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0"></feColorMatrix>
                    <feBlend in2="shape" result="effect1_innerShadow_363_10923"></feBlend>
                </filter>
                <linearGradient
                    id="paint0_linear_363_10923"
                    x1="26.05"
                    x2="0.432"
                    y1="3.167"
                    y2="22.428"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#96AAB7"></stop>
                    <stop offset="1" stopColor="#728896"></stop>
                </linearGradient>
            </defs>
        </svg>
    );
};
