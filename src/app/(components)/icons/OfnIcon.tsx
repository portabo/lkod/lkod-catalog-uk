import * as React from "react";
import { SVGProps } from "react";

const OfnIcon = ({ width = "22", height = "24", ...props }: SVGProps<SVGSVGElement>) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} fill="none" viewBox="0 0 22 24" {...props}>
            <g filter="url(#filter0_i_4300_14550)">
                <path
                    fill="#ED2232"
                    // eslint-disable-next-line max-len
                    d="M21.5 3.75V6c0 2.11-4.734 3.75-10.5 3.75C5.187 9.75.5 8.11.5 6V3.75C.5 1.687 5.188 0 11 0c5.766 0 10.5 1.688 10.5 3.75zm-2.578 6.328c.937-.328 1.875-.797 2.578-1.312V13.5c0 2.11-4.734 3.75-10.5 3.75C5.187 17.25.5 15.61.5 13.5V8.766c.656.515 1.594.984 2.531 1.312 2.11.75 4.922 1.172 7.969 1.172 3 0 5.813-.422 7.922-1.172zm-15.89 7.5c2.109.75 4.921 1.172 7.968 1.172 3 0 5.813-.422 7.922-1.172.937-.328 1.875-.797 2.578-1.312v3.984C21.5 22.36 16.766 24 11 24 5.187 24 .5 22.36.5 20.25v-3.984c.656.515 1.594.984 2.531 1.312z"
                ></path>
            </g>
            <defs>
                <filter
                    id="filter0_i_4300_14550"
                    width="21"
                    height="28"
                    x="0.5"
                    y="0"
                    colorInterpolationFilters="sRGB"
                    filterUnits="userSpaceOnUse"
                >
                    <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
                    <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape"></feBlend>
                    <feColorMatrix
                        in="SourceAlpha"
                        result="hardAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    ></feColorMatrix>
                    <feOffset dy="4"></feOffset>
                    <feGaussianBlur stdDeviation="2"></feGaussianBlur>
                    <feComposite in2="hardAlpha" k2="-1" k3="1" operator="arithmetic"></feComposite>
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0"></feColorMatrix>
                    <feBlend in2="shape" result="effect1_innerShadow_4300_14550"></feBlend>
                </filter>
            </defs>
        </svg>
    );
};

export default OfnIcon;
