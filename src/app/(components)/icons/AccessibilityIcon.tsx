import * as React from "react";
import { SVGProps } from "react";

const AccessibilityIcon = ({ width = "28", height = "22", ...props }: SVGProps<SVGSVGElement>) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} fill="none" viewBox="0 0 28 22" {...props}>
            <g filter="url(#filter0_i_4300_14540)">
                <path
                    fill="#ED2232"
                    // eslint-disable-next-line max-len
                    d="M14.078 6.547c.14 0 .281-.047.422-.047a4.501 4.501 0 010 9A4.471 4.471 0 0110 11v-.375c.422.234.938.375 1.5.375 1.64 0 3-1.313 3-3 0-.516-.188-1.031-.422-1.453zm9.422-2.25c2.203 2.016 3.656 4.453 4.36 6.14.14.376.14.797 0 1.172-.704 1.641-2.157 4.079-4.36 6.141-2.203 2.063-5.25 3.75-9 3.75-3.797 0-6.844-1.688-9.047-3.75-2.203-2.063-3.656-4.5-4.36-6.14a1.68 1.68 0 010-1.172c.704-1.688 2.157-4.126 4.36-6.141C7.656 2.234 10.703.5 14.5.5c3.75 0 6.797 1.734 9 3.797zm-9-.047c-3.75 0-6.75 3.047-6.75 6.75 0 3.75 3 6.75 6.75 6.75 3.703 0 6.75-3 6.75-6.75 0-3.703-3.047-6.75-6.75-6.75z"
                ></path>
            </g>
            <defs>
                <filter
                    id="filter0_i_4300_14540"
                    width="27.047"
                    height="25"
                    x="0.953"
                    y="0.5"
                    colorInterpolationFilters="sRGB"
                    filterUnits="userSpaceOnUse"
                >
                    <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
                    <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape"></feBlend>
                    <feColorMatrix
                        in="SourceAlpha"
                        result="hardAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    ></feColorMatrix>
                    <feOffset dy="4"></feOffset>
                    <feGaussianBlur stdDeviation="2"></feGaussianBlur>
                    <feComposite in2="hardAlpha" k2="-1" k3="1" operator="arithmetic"></feComposite>
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0"></feColorMatrix>
                    <feBlend in2="shape" result="effect1_innerShadow_4300_14540"></feBlend>
                </filter>
            </defs>
        </svg>
    );
};

export default AccessibilityIcon;
