/* eslint-disable max-len */

import * as React from "react";
import { SVGProps } from "react";

const LinkIcon = (props: SVGProps<SVGSVGElement>) => (
    <svg {...props} xmlns="http://www.w3.org/2000/svg" width={14} height={15} fill="none">
        <path
            fill="#036"
            d="M8.75.75h4.375a.9.9 0 0 1 .875.875V6c0 .492-.41.875-.875.875A.864.864 0 0 1 12.25 6V3.758L6.727 9.254a.843.843 0 0 1-1.23 0 .843.843 0 0 1 0-1.23L10.991 2.5H8.75a.864.864 0 0 1-.875-.875c0-.465.383-.875.875-.875Zm-6.563.875H5.25a.9.9 0 0 1 .875.875c0 .492-.41.875-.875.875H2.187a.45.45 0 0 0-.437.438v8.75c0 .246.191.437.438.437h8.75a.45.45 0 0 0 .437-.438V9.5c0-.465.383-.875.875-.875a.9.9 0 0 1 .875.875v3.063c0 1.23-.984 2.187-2.188 2.187h-8.75A2.16 2.16 0 0 1 0 12.562v-8.75C0 2.61.957 1.626 2.188 1.626Z"
        />
    </svg>
);
export default LinkIcon;
