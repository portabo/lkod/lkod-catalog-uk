import * as React from "react";
import { SVGProps } from "react";

import styles from "./IconFills.module.scss";

const ListIcon = ({ width = "32", height = "32", color = "#ED2232", values, fontSize, ...props }: SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        width={width}
        height={height}
        fontSize={fontSize}
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <rect width={width} height={height} rx={16} fill={color} />
        <text
            style={{ fontWeight: "700", fontSize: "14px" }}
            textAnchor="middle"
            fill="#fff"
            x="50%"
            y="50%"
            dominantBaseline="middle"
        >
            {values}
        </text>
    </svg>
);

export default ListIcon;

//x="16" y="21"
