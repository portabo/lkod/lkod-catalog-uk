"use client";
import React, { useEffect, useRef, useState } from "react";

import { Logo } from "@/components/configurable/Logo";
import { NavBar } from "@/components/NavBar";
import Skiplink from "@/components/Skiplink";
import { NavBarConfig } from "@/layout";

import styles from "./Header.module.scss";

type HeaderProps = {
    navBarConfig: NavBarConfig;
    isErrorPage?: boolean;
};

const Header = ({ navBarConfig, isErrorPage }: HeaderProps) => {
    const [isShrunk, setShrunk] = useState(false);
    const logoRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const handler = () => {
            setShrunk((isShrunk) => {
                if (!isShrunk && (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50)) {
                    return true;
                }

                if (isShrunk && document.body.scrollTop < 4 && document.documentElement.scrollTop < 4) {
                    return false;
                }

                return isShrunk;
            });
        };

        window.addEventListener("scroll", handler);
        return () => window.removeEventListener("scroll", handler);
    }, []);

    return (
        <header className={`${styles.header} ${isShrunk ? styles["header_scrolled"] : ""}`}>
            <Skiplink />
            <Logo ref={logoRef} />
            {!isErrorPage && <NavBar navBarConfig={navBarConfig} />}
        </header>
    );
};

export default Header;
