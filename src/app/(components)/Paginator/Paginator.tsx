"use client";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";

import { ChevronIcon } from "@/components/icons/ChevronIcon";
import { useUrlFilter } from "@/hooks/useUrlFilter";
import { objectToQueryString } from "@/utils/objectToQueryString";

import styles from "./Paginator.module.scss";

type PaginatorProps = {
    totalCount: number;
    defaultItemCount: number;
    currentPage: number;
    nItemsWithin?: number;
};

export const Paginator: FC<PaginatorProps> = ({ totalCount, defaultItemCount, currentPage, nItemsWithin = 1 }) => {
    const router = useRouter();
    const pageCount = Math.ceil(totalCount / defaultItemCount) || 1;
    const isPaginationShown = pageCount > 1;
    const isCurrentPageFirst = currentPage === 1;
    const isCurrentPageLast = currentPage === pageCount;

    const { urlFilter, searchString } = useUrlFilter();

    const urlFilterString = objectToQueryString(urlFilter);

    const urlSearchString = searchString ? `&search=${searchString}` : "";

    const onPageNumberClick = (pageNumber: number) => {
        if (pageNumber !== currentPage) {
            router.push(`?${urlFilterString}${urlSearchString}&page=${pageNumber}&perpage=${defaultItemCount}`);
        }
        return;
    };

    const onPreviousPageClick = () => {
        if (!isCurrentPageFirst) {
            router.push(
                `?${urlFilterString}${urlSearchString}&page=${
                    isCurrentPageFirst ? currentPage : currentPage - 1
                }&perpage=${defaultItemCount}`
            );
        }
    };

    const onNextPageClick = () => {
        if (!isCurrentPageLast) {
            router.push(
                `?${urlFilterString}${urlSearchString}&page=${
                    isCurrentPageFirst ? currentPage : currentPage + 1
                }&perpage=${defaultItemCount}`
            );
        }
    };

    const setLastPageAsCurrent = () => {
        if (currentPage > pageCount) {
            router.push(`?${urlFilterString}${urlSearchString}page=${pageCount}`);
        }
    };

    let isPageNumberOutOfRange: boolean;

    const pageNumbers = [...new Array(pageCount)].map((_, index) => {
        const pageNumber = index + 1;

        const isPageNumberFirst = pageNumber === 1;
        const isPageNumberLast = pageNumber === pageCount;
        const isCurrentPageWithinNPageNumbers = Math.abs(pageNumber - currentPage) <= nItemsWithin;

        if (isPageNumberFirst || isPageNumberLast || isCurrentPageWithinNPageNumbers) {
            isPageNumberOutOfRange = false;
            return (
                <button
                    key={pageNumber}
                    className={`${styles["paginator__item"]} ${
                        pageNumber === currentPage ? styles["paginator__item_active"] : ""
                    }`}
                    onClick={() => onPageNumberClick(pageNumber)}
                >
                    {pageNumber}
                </button>
            );
        }

        if (!isPageNumberOutOfRange) {
            isPageNumberOutOfRange = true;
            return <div key={`elipsis${index}`} className={`${styles.elipsis} ${styles.elipsis_disabled}`} />;
        }

        return null;
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(setLastPageAsCurrent, [currentPage, pageCount]);

    return (
        <>
            {isPaginationShown && (
                <div className={styles.paginator}>
                    <button
                        key={`left`}
                        aria-label={"Předchozí stránka"}
                        onClick={onPreviousPageClick}
                        className={styles.arrow}
                        type="button"
                    >
                        <ChevronIcon color="secondary" direction={`left`} />
                    </button>
                    {pageNumbers}
                    <button
                        key={`right`}
                        aria-label={"Následující stránka"}
                        onClick={onNextPageClick}
                        className={styles.arrow}
                        type="button"
                    >
                        <ChevronIcon color="secondary" direction={`right`} />
                    </button>
                </div>
            )}
        </>
    );
};
