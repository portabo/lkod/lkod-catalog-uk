import React from "react";

import styles from "./Skiplink.module.scss";

const Skiplink = () => {
    return (
        <a className={styles["skip-link"]} href="#main">
            Přeskočit na hlavní obsah
        </a>
    );
};

export default Skiplink;
