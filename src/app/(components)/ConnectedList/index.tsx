import React, { ReactNode } from "react";

import styles from "./ConnectedList.module.scss";

export const ConnectedList = ({ children }: { children: ReactNode }) => {
    return <ul className={styles["connected-list"]}>{children}</ul>;
};
