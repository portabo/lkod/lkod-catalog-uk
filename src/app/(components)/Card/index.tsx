/* eslint-disable react/prop-types */
import React, {
    AnchorHTMLAttributes,
    ButtonHTMLAttributes,
    DetailedHTMLProps,
    ForwardedRef,
    forwardRef,
    HTMLAttributes,
} from "react";

import styles from "./Card.module.scss";

type Tag = "a" | "button" | "div";

type CommonProps = {
    tag: Tag;
    className?: string;
    horizontal?: "true" | "false";
    label?: string;
};

type CardProps = CommonProps & DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> & { href?: undefined };

type CardButtonProps = CommonProps &
    DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & { href?: undefined };

type CardAnchorProps = DetailedHTMLProps<AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement> & {
    tag: Tag;
    className?: string;
    horizontal?: "true" | "false";
    href: string;
};

type PolymorphicProps = CardProps | CardButtonProps | CardAnchorProps;

type PolymorphicCard = {
    (props: CardProps): JSX.Element;
    (props: CardButtonProps): JSX.Element;
    (props: CardAnchorProps): JSX.Element;
};

const isDiv = (props: PolymorphicProps): props is CardProps => {
    return props.href === undefined && props.tag === "div";
};

const isButton = (props: PolymorphicProps): props is CardButtonProps => {
    return props.href === undefined && props.tag === "button";
};

export const Card = forwardRef<HTMLDivElement | HTMLButtonElement | HTMLAnchorElement, PolymorphicProps>(
    (props: PolymorphicProps, ref) => {
        const classes = `${styles["card"]} ${props.horizontal === "true" ? styles["card_horizontal"] : ""} ${
            props.tag === "a" || props.tag === "button" ? styles["card-active"] : ""
        } ${props.className}`;

        return isDiv(props) ? (
            <div {...props} ref={ref as ForwardedRef<HTMLDivElement>} className={classes} />
        ) : isButton(props) ? (
            <button
                {...props}
                ref={ref as ForwardedRef<HTMLButtonElement>}
                className={classes}
                aria-label={props.label ?? ""}
                type="button"
            />
        ) : (
            <a {...props} ref={ref as ForwardedRef<HTMLAnchorElement>} className={classes} />
        );
    }
) as PolymorphicCard;
