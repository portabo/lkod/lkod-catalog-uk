import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import styles from "./HorizontalLine.module.scss";

interface IHorizontalLine extends DetailedHTMLProps<HTMLAttributes<HTMLHRElement>, HTMLHRElement> {
    className?: string;
}

const HorizontalLine: FC<IHorizontalLine> = ({ className }) => {
    return <hr className={`${styles.hr} ${className}`} />;
};

export default HorizontalLine;
