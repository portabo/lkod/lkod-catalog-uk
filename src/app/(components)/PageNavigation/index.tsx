"use client";
import { usePathname, useRouter } from "next/navigation";
import React from "react";

import { useUrlFilter } from "@/hooks/useUrlFilter";
import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import { objectToQueryString } from "@/utils/objectToQueryString";

import Button from "../Button";
import { Paginator } from "../Paginator/Paginator";

type PageNavigationProps = {
    defaultPerPage: number;
    page: string;
    perpage: string;
    count: number;
};

export const PageNavigation = ({ defaultPerPage, page, perpage, count }: PageNavigationProps) => {
    const router = useRouter();
    const pathname = usePathname();
    const size: ISize = useWindowSize();
    const currentPage = page ? Number(page) : 1;
    const currentPerPage = perpage ? Number(perpage) : defaultPerPage;
    const pageCount = Math.ceil(count / defaultPerPage) || 1;
    const isCurrentPageLast = currentPage === pageCount;

    const { urlFilter, searchString } = useUrlFilter();

    const urlFilterString = objectToQueryString(urlFilter);

    const urlSearchString = searchString ? `&search=${searchString}` : "";

    const perPageHandler = () => {
        const lastItem = currentPage * defaultPerPage;
        router.push(
            `${pathname}?${urlFilterString}${urlSearchString}&page=${
                lastItem < count && lastItem + defaultPerPage < count ? currentPage + 1 : pageCount
            }&perpage=${!isCurrentPageLast ? currentPerPage + defaultPerPage : currentPerPage}`,
            { scroll: false }
        );
    };

    return (
        <>
            {count > defaultPerPage && !isCurrentPageLast && (
                <Button color="primary" label="Zobrazit další" onClick={perPageHandler} />
            )}
            <Paginator
                totalCount={count}
                defaultItemCount={defaultPerPage}
                currentPage={currentPage}
                nItemsWithin={size.width && size.width > 768 ? (size.width > 1200 ? 5 : 4) : 1}
            />
        </>
    );
};
