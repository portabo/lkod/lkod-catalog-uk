import Link from "next/link"; // Assuming you're using Next.js
import { MouseEventHandler, ReactNode, RefObject, useEffect, useRef, useState } from "react";

import { ChevronIcon } from "../icons/ChevronIcon";
import styles from "./Dropdown.module.scss";

interface IDropdown {
    children: ReactNode;
    title: string;
    href: string;
    isOpen?: boolean;
    toggleHandlerDropDown: MouseEventHandler<HTMLAnchorElement>;
    className?: string;
}
export const NavbarDropdown = ({ children, title, href, className, toggleHandlerDropDown }: IDropdown) => {
    const [isOpen, setIsOpen] = useState(false);
    const dropdownRef: RefObject<HTMLLIElement> = useRef(null);
    const dropdownToggleHandler = () => {
        setIsOpen(!isOpen);
    };

    useEffect(() => {
        if (toggleHandlerDropDown.length) {
            setIsOpen(false);
        }
    }, [toggleHandlerDropDown]);

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
                setIsOpen(false);
            }
        };
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
    return (
        <li className={styles["navbar-dropdown"]} ref={dropdownRef}>
            <div className={styles["dropdown-container"]}>
                <Link href={href} onClick={toggleHandlerDropDown} className={className}>
                    {title}
                </Link>
                <button
                    type="button"
                    className={styles["dropdown-container-button"]}
                    onClick={dropdownToggleHandler}
                    aria-label="Rozbalovací nabídka navigace"
                >
                    <ChevronIcon direction={isOpen ? "down" : "up"} color="black" width="13" />
                </button>
            </div>
            <div className={isOpen ? styles["show"] : styles["dropdown-content"]}>{children}</div>
        </li>
    );
};
