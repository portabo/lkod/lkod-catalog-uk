import { FC, ReactNode } from "react";

import styles from "./IconFrame.module.scss";

const IconFrame: FC<{ children: ReactNode }> = ({ children }) => {
    return <span className={styles["icon-frame"]}>{children}</span>;
};

export default IconFrame;
