"use client";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { ComponentType } from "react";

import { CzechFlagIcon } from "@/components/icons/CzechFlagIcon";
import { EnglishFlagIcon } from "@/components/icons/EnglishFlagIcon";

import style from "./Langswitch.module.scss";

export const LangSwitch = () => {
    const router = useRouter();
    const pathname = usePathname();
    const searchParams = useSearchParams();

    const lang = searchParams.get("lang") || "cz";

    const handleLangSwitch = () => {
        lang === "cz" ? router.push("?lang=en") : router.push(pathname);
        router.refresh();
    };

    const label: { icon: { [key: string]: ComponentType }; text: { [key: string]: string } } = {
        icon: {
            cz: EnglishFlagIcon,
            en: CzechFlagIcon,
        },
        text: {
            cz: "Switch to EN",
            en: "Přepnout na CZ",
        },
    };

    const Icon = label.icon[lang];

    return (
        <button className={style["langswitch-btn"]} onClick={handleLangSwitch}>
            <Icon />
            {label.text[lang]}
        </button>
    );
};
