import React, { MouseEvent } from "react";

import { ChevronIcon } from "@/components/icons/ChevronIcon";

import styles from "./ListLimiter.module.scss";

type Props = { limit: number; rest: number; onClick: (e: MouseEvent<HTMLButtonElement>) => void };

export const ListLimiter = ({ limit, rest, onClick }: Props) => {
    return rest <= 0 ? (
        <button className={styles["list-limiter"]} onClick={onClick} type="button">
            <ChevronIcon color="secondary" direction={`up`} />
            {`Zobrazit ${limit > 4 ? "prvních" : "první"}`}
            <span>{limit}</span>
        </button>
    ) : (
        <button className={styles["list-limiter"]} onClick={onClick} type="button">
            <ChevronIcon color="secondary" direction={`down`} />
            {`Zobrazit ${rest > 4 ? "dalších" : "další"}`}
            <span>{rest}</span>
        </button>
    );
};
