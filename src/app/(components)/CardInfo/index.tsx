import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import styles from "./CardInfo.module.scss";

type Props = DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

export const CardInfo: FC<Props> = ({ children, className }) => {
    return <div className={`${styles["info-card"]} ${className}`}>{children}</div>;
};
