"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { forwardRef, KeyboardEvent, useEffect, useState } from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { SearchInput } from "@/components/search/SearchInput";
import text from "@/textContent/cs.json";

import styles from "./SearchForm.module.scss";

type SearchFormProps = {
    label?: string;
    fullWidth?: boolean;
    placeholder?: string;
};

export const SearchForm = forwardRef<HTMLDivElement, SearchFormProps>((SearchFormProps, ref) => {
    const { label, fullWidth, placeholder = `${text.searchString.placeholderText}` } = SearchFormProps;
    const router = useRouter();
    const search = useSearchParams()?.get("search");
    const plausible = usePlausible();

    const [inputValue, setInputValue] = useState("");

    const updateInputValue = (value: string) => {
        setInputValue(value);
    };

    const searchClicked = () => {
        router.push(`/datasets?search=${encodeURIComponent(inputValue)}`);
        plausible("SearchEvent: Search", {
            props: {
                inputValue,
            },
        });
    };

    const onKeyHandler = (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            searchClicked();
        }
    };

    const resetInputField = () => {
        updateInputValue("");
        router.push(`?`);
    };

    useEffect(() => {
        if (!search) {
            setInputValue("");
        } else {
            setInputValue(search as string);
        }
    }, [search]);

    return (
        <div className={`${styles["search-form"]} ${fullWidth ? styles.full : ""}`} ref={ref}>
            <div className={styles.row}>
                {label ? (
                    <Heading tag={`h2`} type={`h4`}>
                        {label}
                    </Heading>
                ) : (
                    ""
                )}
            </div>
            <div className={styles.row}>
                <SearchInput
                    name="dataSearch"
                    aria-labelledby="searchButton"
                    placeholder={placeholder}
                    onKeyDown={onKeyHandler}
                    resetInputField={resetInputField}
                    updateInputValue={updateInputValue}
                    value={inputValue}
                />
                <Button id="searchButton" color="primary" label="Vyhledat" onClick={searchClicked} />
            </div>
        </div>
    );
});
