import Image from "next/image";
import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import styles from "./IconExtSource.module.scss";

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    src: string;
    alt: string;
    width?: number;
    height?: number;
}

const IconExtSource: FC<Props> = ({ src, alt, width = 24, height = 24 }) => {
    return (
        <span className={styles["icon-external"]}>
            <Image src={src} width={width} height={height} alt={alt} />
        </span>
    );
};

export default IconExtSource;
