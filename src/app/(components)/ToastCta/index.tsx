"use client";
import { useEffect, useState } from "react";

import text from "@/textContent/cs.json";

import { ButtonReset } from "../ButtonReset";
import ArrowIcon from "../icons/ArrowIcon";
import CTAButtonIcon from "../icons/CTAButtonIcon";
import styles from "./ToastCta.module.scss";

type ToastProps = {
    message: string;
    url?: string;
};

const ToastCta = ({ message, url }: ToastProps) => {
    const [isVisible, setIsVisible] = useState(false);
    const [hasBeenShown, setHasBeenShown] = useState(
        typeof window !== "undefined" ? localStorage.getItem("hasBeenShown") : false
    );

    useEffect(() => {
        if (!hasBeenShown) {
            setIsVisible(true);
        }
    }, [hasBeenShown]);

    const handleClose = () => {
        setIsVisible(false);
        setHasBeenShown(true);
        localStorage.setItem("hasBeenShown", true.toString());
    };

    return (
        <>
            {isVisible && (
                <div className={styles.toast}>
                    <div className={styles["toast__header"]}>
                        <a href={url} target="_blank" rel="noreferrer" onClick={handleClose}>
                            <span className={styles["toast__header-content"]}>
                                <CTAButtonIcon color={`#5296D5`} width={22} height={22} />
                                <span className={styles["toast__header-message"]}>{message}</span>
                                <ArrowIcon />
                            </span>
                        </a>
                        <ButtonReset
                            label={text.buttons.close}
                            hideLabel
                            onClick={() => handleClose()}
                            className={styles["toast__closeButton"]}
                        />
                    </div>
                    <div className={styles["toast__ctaDescription"]}>{text.cta.ctaDescription}</div>
                </div>
            )}
        </>
    );
};

export default ToastCta;
