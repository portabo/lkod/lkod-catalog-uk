import Link from "next/link";
import { usePathname } from "next/navigation";
import React, { MouseEventHandler, use, useState } from "react";

import { BurgerIcon } from "@/components/icons/BurgerIcon";
import { CloseIcon } from "@/components/icons/CloseIcon";
import { useOrganizationsPromise } from "@/context/OrganizationsProvider";
import { NavBarConfig } from "@/layout";
import text from "@/textContent/cs.json";

import { NavbarDropdown } from "../NavbarDropdown";
import styles from "./NavBar.module.scss";

type NavBarProps = {
    navBarConfig: NavBarConfig;
};

export const NavBar = ({ navBarConfig }: NavBarProps) => {
    const organizations = use(useOrganizationsPromise());
    const pathname = usePathname();

    const [isOpenDropDown, setIsOpenDropDown] = useState<string | null>("");

    const showOrgs = organizations.length > 0;

    const [open, setOpen] = useState(false);

    const toggleHandler = () => {
        setOpen(!open);
    };
    const toggleHandlerDropDown: MouseEventHandler<HTMLAnchorElement> = (e) => {
        if (e.target) {
            setIsOpenDropDown(e.currentTarget.innerText);
            toggleHandler();
        }
        isOpenDropDown;
    };
    const resetFiltersAndSearch = () => {
        toggleHandler();
    };
    return (
        <nav className={styles.navbar}>
            <button className={styles["navbar__button"]} onClick={toggleHandler} aria-label={`Menu`} type="button">
                {open ? <CloseIcon color={`tertiary`} /> : <BurgerIcon color={"tertiary"} />}

                <span className={styles["navbar__title"]}>MENU</span>
            </button>
            <div className={`${styles["navbar__menu"]} ${open ? styles["open"] : ""}`}>
                <ul>
                    <li>
                        <Link href="/" className={pathname === "/" ? styles.active : ""} onClick={toggleHandler}>
                            {text.navBar.home}
                        </Link>
                    </li>
                    <li>
                        <Link
                            href="/datasets"
                            className={pathname?.includes("/datasets") ? styles.active : ""}
                            onClick={resetFiltersAndSearch}
                        >
                            {text.navBar.datasets}
                        </Link>
                    </li>
                    {showOrgs ? (
                        <li>
                            <Link
                                href="/organizations"
                                className={pathname?.includes("/organizations") ? styles.active : ""}
                                onClick={toggleHandler}
                            >
                                {text.navBar.organizations}
                            </Link>
                        </li>
                    ) : (
                        false
                    )}
                    {navBarConfig.showProjectPage && (
                        <NavbarDropdown
                            title={text.navBar.aboutCatalog}
                            href="/about-open-data"
                            isOpen
                            className={pathname?.includes("/about-open-data") ? styles.active : ""}
                            toggleHandlerDropDown={toggleHandlerDropDown}
                        >
                            <ul className={styles["dropdown-list"]}>
                                <li className={styles.dropdown}>
                                    <Link href="/about-open-data/what-is-open-data" onClick={toggleHandlerDropDown}>
                                        {text.navBar.whatIsOD}
                                    </Link>
                                </li>
                                <li className={styles.dropdown}>
                                    <Link href="/about-open-data/open-data-use-and-contribution" onClick={toggleHandlerDropDown}>
                                        {text.navBar.useAndBenefitOD}
                                    </Link>
                                </li>
                                <li className={styles.dropdown}>
                                    <Link href="/about-open-data/open-data-publish" onClick={toggleHandlerDropDown}>
                                        {text.navBar.HowToPublishOD}
                                    </Link>
                                </li>
                                <li className={`${styles.dropdown} ${styles.disabled}`}>
                                    <Link href="/" onClick={toggleHandlerDropDown}>
                                        {text.navBar.EducationAndLinks}
                                    </Link>
                                </li>
                            </ul>
                        </NavbarDropdown>
                    )}
                    {navBarConfig.showAboutLkodPage !== "false" && (
                        <li>
                            <Link
                                href="/about-lkod"
                                className={pathname?.includes("/about-lkod") ? styles.active : ""}
                                onClick={toggleHandler}
                            >
                                {text.navBar.aboutLKOD}
                            </Link>
                        </li>
                    )}
                    {navBarConfig.showDataPrahaPage !== "false" && (
                        <li>
                            <Link href="https://data.praha.eu/" onClick={toggleHandler}>
                                {text.navBar.dataPraha}
                            </Link>
                        </li>
                    )}
                </ul>
            </div>
        </nav>
    );
};
