import { DetailedHTMLProps, HTMLAttributes } from "react";

import styles from "./ButtonReset.module.scss";

type Colors = "primary" | "secondary" | "gray-dark";

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    color?: Colors;
    label?: string;
    hideLabel?: boolean;
    end?: boolean;
    className?: string;
}

export const ButtonReset = ({ className, color, label, hideLabel, onClick, end }: Props) => {
    return (
        <button
            aria-label={label}
            type="reset"
            className={`${styles["reset-button"]} ${color ? styles[`reset-button_color-${color}`] : ""} ${
                end ? styles["reset-button_end"] : ""
            } ${className ?? ""}`}
            onClick={onClick}
        >
            {!hideLabel && <span>{label}</span>} &times;
        </button>
    );
};
