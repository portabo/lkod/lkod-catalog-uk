import React, { FC, memo } from "react";

import { Paragraph } from "@/components/Paragraph";
import facebook from "@/images/facebook.svg";
import ikorss from "@/images/iko-rss.svg";
import ikolin from "@/images/iko-lin.svg";
import text from "@/textContent/cs.json";

import SocialIcon from "../SocialIcon/SocialIcon";
import styles from "./Socials.module.scss";

const socialIcons = [
    { name: "Facebook", url: facebook, link: "https://www.facebook.com/usteckykrajzive/" },
    { name: "RSS", url: ikorss, link: "https://www.kr-ustecky.cz/rss" },
    { name: "LinkedIn", url: ikolin, link: "https://www.linkedin.com/company/krajský-úřad-ústeckého-kraje" },
];

const Socials: FC = () => {
    return (
        <div className={styles.socials}>
            <Paragraph>{text.footer.socialsHeading}</Paragraph>
            <div className={styles["social-icons"]}>
                {socialIcons.map((icon, i) => {
                    return <SocialIcon label={icon.name} url={icon.url} link={icon.link} key={i} />;
                })}
            </div>
        </div>
    );
};

export default memo(Socials);
