"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React, { memo } from "react";

import { Logo } from "@/components/configurable/Logo";
import Socials from "@/components/Footer/Socials";
import HorizontalLine from "@/components/HorizontalLine";
import { LoginIcon } from "@/components/icons/LoginIcon";
import text from "@/textContent/cs.json";

import styles from "./Footer.module.scss";
import Logos from "./Logos/Logos";
import Logos2 from "./Logos2/Logos";

type Props = {
    adminDomain: string;
    showAdminLink: string;
};

const Conditions = ({ adminDomain, showAdminLink }: Props) => {
    return (
        <div className={styles.conditions}>
            <Link href="/cookies">{text.privacyPolicyPage.pageTitle}</Link>
            <Link href="/accessibility">{text.footer.accessibilityLink}</Link>
            {showAdminLink !== "false" && (
                <Link href={`${adminDomain}/login`} className={styles["red-link"]}>
                    {text.footer.login}
                    <LoginIcon color="primary" />
                </Link>
            )}
        </div>
    );
};

const Footer = ({ adminDomain, showAdminLink }: Props) => {
    const pathname = usePathname();
    const onHomepage = pathname === "/";

    return (
        <footer className={`${styles.footer} ${onHomepage ? styles["footer-homepage"] : ""}`}>
            <div className={styles["container"]}>
                <Logos />
                <Logos2 />
                <Socials />
                <HorizontalLine className={styles["top-line"]} />
            </div>
            <div className={styles["container"]}>
                <Conditions adminDomain={adminDomain} showAdminLink={showAdminLink} />
                <HorizontalLine className={styles["bottom-line"]} />
                <div className={styles["logo-container"]}>
                    <Logo footer />
                </div>
            </div>
        </footer>
    );
};

export default memo(Footer);
