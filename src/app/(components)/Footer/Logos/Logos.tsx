import React, { memo } from "react";

// import { Paragraph } from "@/components/Paragraph";
import logo_dcuk from "@/images/logos/dcuk.svg";
import logo_eu from "@/images/logos/eu_logo.svg";
// import logo_golemio from "@/images/logos/logo_golemi_gray.svg";
// import logo_oict from "@/images/logos/logo_oict_gray.svg";
import logo_tcuk from "@/images/logos/tcuk_logo.svg";
import logo_portaluk from "@/images/logos/uk-logo-01.svg";
// import text from "@/textContent/cs.json";


import SocialIcon from "../SocialIcon/SocialIcon";
import styles from "./Logos.module.scss";

const logos = [
    { name: "Logo EU", url: logo_eu, link: "" },

    { name: "Logo Portál Ústeckého kraje", url: logo_portaluk, link: "https://www.kr-ustecky.cz/" },
    { name: "Datové centrum ústeckého kraje", url: logo_dcuk, link: "https://dcuk.cz" },
    { name: "Transformační centrum ústeckého kraje", url: logo_tcuk, link: "https://tcuk.cz" }
,
];

const Logos = () => {
    return (
        <div id="flogorow1" className={styles["logos-container"]}>
            <Paragraph>{text.footer.logosHeading}</Paragraph>
            <div className={styles["logos"]}>
                {logos.map((icon, i) => {
                    return <SocialIcon label={icon.name} url={icon.url} link={icon.link} key={i} isLogo />;
                })}
            </div>
        </div>
    );
};

export default memo(Logos);
