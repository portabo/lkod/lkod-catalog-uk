import React, { memo } from "react";

// import { Paragraph } from "@/components/Paragraph";
// import logo_eu from "@/images/logos/eu_logo.svg";
import logo_golemio from "@/images/logos/logo_golemi_gray.svg";
import logo_oict from "@/images/logos/logo_oict_gray.svg";
// import logo_portaluk from "@/images/logos/uk-logo-01.svg";

// import text from "@/textContent/cs.json";
import SocialIcon from "../SocialIcon/SocialIcon";
import styles from "./Logos.module.scss";

const logos = [
    { name: "Logo OICT", url: logo_oict, link: "https://operatorict.cz/" },
    { name: "Logo Golemio", url: logo_golemio, link: "https://golemio.cz/" },
];

const Logos2 = () => {
    return (
        <div id="logorow2" className={styles["logos-container"]}>
            <div className={styles["logos"]}>
                {logos.map((icon, i) => {
                    return <SocialIcon label={icon.name} url={icon.url} link={icon.link} key={i} isLogo />;
                })}
            </div>
        </div>
    );
};

export default memo(Logos2);
