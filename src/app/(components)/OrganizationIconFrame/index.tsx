import Image from "next/image";
import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import { Heading } from "@/components/Heading";

import styles from "./OrganizationIconFrame.module.scss";
interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    src: string | null;
    alt: string;
    name: string;
    description?: string | null;
}

const OrgIconFrame: FC<Props> = ({ src, alt, name, description }) => {
    return (
        <div className={styles["frame-wrapper"]}>
            <p>{"Organizace"}</p>
            <div className={styles["icon-box"]}>{src && <Image src={src} fill alt={alt} />}</div>
            <Heading tag={`h2`} type={`h5`}>
                {name}
            </Heading>
            <p>{description}</p>
        </div>
    );
};

export default OrgIconFrame;
