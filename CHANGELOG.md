# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.9] - 2025-01-07

### Changed

- Update node  to v20.18.0 ([lkod-catalog#135](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/135))

## [1.2.8] - 2024-12-19

### Added

- Added exploratory analysis ([lkod-catalog#133](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/133))

## [1.2.7] - 2024-11-04

### Changed

- Error and debug logging improved, minor fixes on error boundaries, packages updated ([lkod-catalog#13](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/issues/13))

## [1.2.6] - 2024-09-17

### Added

- Added notification banner configurable by environment variables ([lkod-catalog#130](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/130))

## [1.2.5] - 2024-08-19

### Added

- add backstage metadata files
- add .gitattributes file

## [1.2.4] - 2024-08-15

### Added

- Added unit testing with Jest and React Testing Library ([lkod-catalog#12](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/issues/12))

## [1.2.3] - 2024-07-25

### Changed

- About LKOD page image updated to recent system structure

## [1.2.2] - 2024-06-20

### Fixed

- Fix sitemap ulr([lkod-catalog#127](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/127))

## [1.2.1] - 2024-06-19

### Added

- Aboud LKOd page - english mutation ([lkod-catalog#125](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/125))

## [1.2.0] - 2024-05-29

### Added

- Added ability to fetch data from new API instead of SPARQL ([lkod-catalog#107](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/107))

### Changed

- node version updated to 20.11

### Fixed

- remove "?download=true" from downloadFile function ([lkod-catalog#124](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/124))

## [1.1.3] - 2024-04-25

### Fixed

- improve api-client logging

## [1.1.2] - 2024-02-29

### Fixed

- fixed bug with chached organizations ([lkod-catalog#11](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/issues/11))

## [1.1.1] - 2024-02-01

### Changed

- Env variables changed back to `PUBLIC_URL, ADMIN_URL, SHOW_PROJECT_PAGE, SHOW_ABOUTLKOD_PAGE, SHOW_ADMINLOGIN_LINK` and `SHOW_DATAPRAHA_PAGE`

## [1.1.0] - 2024-01-31

### Changed

- Next.js updated to version 14, env variables changed to `NEXT_PUBLIC_URL, NEXT_PUBLIC_ADMIN_URL, NEXT_PUBLIC_SHOW_PROJECT_PAGE, NEXT_PUBLIC_SHOW_ABOUTLKOD_PAGE, NEXT_PUBLIC_SHOW_ADMINLOGIN_LINK` and added `NEXT_PUBLIC_SHOW_DATAPRAHA_PAGE`, because of navigation changes ([lkod-catalog#118](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/118))

## [1.0.8] - 2024-01-03

### Added

- Added plausible monitoring of 404 page ([lkod-catalog#15](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/issues/15))

## [1.0.7] - 2023-10-23

### Changed

- Footer redesigned

## [1.0.6] - 2023-10-09

### Added

- Added availability to search by publisher name ([lkod-catalog#111](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/111))

## [1.0.5] - 2023-09-18

### Added

- Add new page "Co jsou otevřená data", "Využití a přínos otevřených dat" and "Jak publikovat otevřená data"

## [1.0.4] - 2023-09-14

### Added

- Logging with pino library on server and client side

### Changed

- Data fetching with axios library chaged to fetch Api

## [1.0.3] - 2023-07-25

### Changed

- Fixed sitemap. Add organization pages ([lkod-catalog#102](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/102))

## [1.0.2] - 2023-07-11

### Changed

- Site layout change according to the source SPARQL sending topics and publisher metadata ([lkod-catalog#9](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/issues/9))

## [1.0.1] - 2023-07-03

### Added

- Organization page URL ([lkod-catalog#94](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/94))

## [1.0.0] - 2023-06-27

### Added

- Add `CHANGELOG.md` file

### Changed

- New oict logo

